# Description

## Double Pendulum 

Folder contains following subfolders:

* Custom layers required for training
* NN Data including NNs themselves and appropriate tests outlined in the paper
* Raw format of plots presented in the paper
* Scripts required for NN training

## Duffing Oscillator

Folder contains following subfolders:

* Folder containing refined loading extrapolation results
* Models required for data acquisition for NN training and NN results comparison
* NN Data including NNs themselves and appropriate tests outlined in the paper
* Raw format of plots presented in the paper
* Scripts required for NN training
* Specific training scripts required for NN training

## Gyroscope

Folder contains following subfolders:

* Models required for data acquisition for NN training and NN results comparison
* NN Data including NNs themselves and appropriate tests outlined in the paper
* Raw format of plots presented in the paper
* Scripts used for searching propper hyperparameters for NN
* Results from running hyperparameter searches for a "parallel" NN structure (top 20 best search results)
* Scripts required for NN training