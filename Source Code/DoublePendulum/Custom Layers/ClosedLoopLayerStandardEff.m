classdef ClosedLoopLayerStandardEff < nnet.layer.Layer % ...
    properties
        stepAhead
    end

    properties (Learnable)
        l1Weights
        l2Weights
        l3Weights

        l1Bias
        l2Bias
        l3Bias
    end
    methods

        function layer = ClosedLoopLayerStandardEff(l1,l2,stepAhead)
            %% Weight initialization via glorot
            layer.l1Weights = myGlorot(l1,4);
            layer.l2Weights = myGlorot(l2,l1);
            layer.l3Weights = myGlorot(4,l2);
            %% Bias initialization to 0
            layer.l1Bias = dlarray(zeros(l1,1));
            layer.l2Bias = dlarray(zeros(l2,1));
            layer.l3Bias = dlarray(zeros(4,1));

            layer.stepAhead = stepAhead;

            
        end

        function Z = predict(layer,X)
            if (size(X,3) == 1)
                Z = X(1:4,:,:);
                Z = applyNetFast(layer,X);
            else
                numICs = size(X,3)/layer.stepAhead;
                offset = 0;
    
                %ICs = dlarray(zeros(size(X(:,:,1:numICs))));
                ICs = X(:,:,1:numICs);
                Z = X(1:4,:,:);
    
                Z(1:4,:,1+offset:numICs+offset) = applyNetFast(layer,ICs);
    
                for i=2:layer.stepAhead
                    offset = offset + numICs;
                    Z(1:4,:,1+offset:numICs+offset) = applyNetFast(layer,Z(1:4,:,1+offset-numICs:offset));
                end
            end
        end

        function Z = forward(layer,X)
            if (size(X,3) == 1)
                Z = X(1:4,:,:);
                Z = applyNetFast(layer,X);
            else
                numICs = size(X,3)/layer.stepAhead;
                offset = 0;
    
                %ICs = dlarray(zeros(size(X(:,:,1:numICs))));
                ICs = X(:,:,1:numICs);
                Z = X(1:4,:,:);
    
                Z(1:4,:,1+offset:numICs+offset) = applyNetFast(layer,ICs);
    
                for i=2:layer.stepAhead
                    offset = offset + numICs;
                    Z(1:4,:,1+offset:numICs+offset) = applyNetFast(layer,Z(1:4,:,1+offset-numICs:offset));
                end
                Zd = extractdata(squeeze(Z));
            end
        end
    end
end

function output = applyNetFast(layer,X)
    %% First Layer
    temp = pagemtimes(layer.l1Weights,X);
    temp = temp + layer.l1Bias;
    %% ReLU
    temp = max(0,temp);
    %% Second Layer
    temp = pagemtimes(layer.l2Weights,temp);
    temp = temp + layer.l2Bias;
    %% Third Layer
    temp = pagemtimes(layer.l3Weights,temp);
    temp = temp + layer.l3Bias;

    output = temp;
end

function weights = myGlorot(numOut,numIn)
    sz = [numOut numIn];
    Z = 2*rand(sz,'single') - 1;
    bound = sqrt(6 / (numIn + numOut));
    weights = bound * Z;
    weights = dlarray(weights);
end
