classdef ClosedLoopLayerStandard < nnet.layer.Layer % ...
    properties
        stepAhead
    end

    properties (Learnable)
        l1Weights
        l2Weights
        l3Weights

        l1Bias
        l2Bias
        l3Bias
    end
    methods

        function layer = ClosedLoopLayerStandard(l1w,l2w,l3w,l1b,l2b,l3b,stepAhead)
            %% Weight initialization via glorot
            layer.l1Weights = l1w;
            layer.l2Weights = l2w;
            layer.l3Weights = l3w;
            %% Bias initialization to 0
            layer.l1Bias = l1b;
            layer.l2Bias = l2b;
            layer.l3Bias = l3b;

            layer.stepAhead = stepAhead;
        end

        function Z = predict(layer,X)
            len = size(X,3);
            currState = dlarray(zeros(size(X,1),1));
            Z = X(1:4,:,:);
            for i=1:len
                rest = mod(i,layer.stepAhead);
                if rest==1
                    currState = X(:,:,i);
                end
                Z(:,:,i) = applyNetFast(layer,currState);
                currState = Z(:,:,i);
            end
        end

        function Z = forward(layer,X)
            len = size(X,3);
            currState = dlarray(zeros(size(X,1),1));
            Z = X(1:4,:,:);
            for i=1:len
                rest = mod(i,layer.stepAhead);
                if rest==1
                    currState = X(:,:,i);
                end
                Z(:,:,i) = applyNetFast(layer,currState);
                currState = Z(:,:,i);
            end
        end
    end
end

function output = applyNetFast(layer,X)
    %% First Layer
    temp = layer.l1Weights*X;
    temp = temp + layer.l1Bias;
    %% ReLU
    temp = max(0,temp);
    %% Second Layer
    temp = layer.l2Weights*temp;
    temp = temp + layer.l2Bias;
    %% Third Layer
    temp = layer.l3Weights*temp;
    temp = temp + layer.l3Bias;

    output = temp;
end

