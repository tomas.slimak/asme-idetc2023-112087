# Description

Folder contains following subfolders:

* Custom layers required for training
* NN Data including NNs themselves and appropriate tests outlined in the paper
* Raw format of plots presented in the paper
* Scripts required for NN training