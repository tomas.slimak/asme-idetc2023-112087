clc; cla;
g = 9.81;
m_2 = 2;
m_1 = 2;
l_1 = 2;
l_2 = 3;

max_t = 5;
snapsPerS = 100;

%load('DPdnnStandard.mat');

load('DPdnnStandardRetrain100Hz5s.mat');

%netOld = net;

double_penudlum = @(t,x)double_pendulum_system(x,l_1,l_2,m_1,m_2,g);

%train_net_grid(double_penudlum,max_t,snapsPerS)

%retrain_net_grid(double_penudlum,max_t,snapsPerS,netOld)

test_net_grid(double_penudlum,net,max_t,snapsPerS,th1,th2)

%test_net_grid_performance(double_penudlum,net,max_t,snapsPerS,th1,th2)

function train_net_grid(double_penudlum,max_t,snapsPerS)

    th1 = 0:1:4;
    th2 = -5:1:5;

    [input,output] = generate_data(th1,th2,max_t,snapsPerS,double_penudlum);
    partition = floor(0.8*size(input,2));

    inputT = input(:,1:partition-1);
    outputT = output(:,1:partition-1);

    inputV = input(:,partition:end);
    outputV = output(:,partition:end);

    maxEpochs = 2000;
    options = trainingOptions('adam', ...
    'ExecutionEnvironment','cpu', ...
    'MaxEpochs',maxEpochs, ...
    'OutputNetwork','best-validation-loss',...
    'ValidationFrequency',10, ...
    'Shuffle','every-epoch', ...
    'LearnRateSchedule','piecewise', ...
    'GradientDecayFactor',0.90,...
    'SquaredGradientDecayFactor',0.99,...
    'InitialLearnRate',0.0260,...
    'LearnRateDropFactor',0.7848, ...
    'LearnRateDropPeriod',103, ...
    'L2Regularization',0.2,...
    'MiniBatchSize',128,...
    'ValidationData',{inputV,outputV});
    

    layers = [ ...
    sequenceInputLayer(4)
    fullyConnectedLayer(47)
    reluLayer
    fullyConnectedLayer(60)
    fullyConnectedLayer(4)
    regressionLayer
    ];

    [net,info] = trainNetwork(inputT,outputT,layers,options); 
    save(['DPdnnStandard','.mat'],'net','info','options','th1','th2','max_t','snapsPerS','layers');
    
end

function retrain_net_grid(double_penudlum,max_t,snapsPerS,netOld)

    th1 = 0:1:4;
    th2 = -5:1:5;

    [input,output] = generate_data(th1,th2,max_t,snapsPerS,double_penudlum);

    SAhead = max_t*snapsPerS;

    numSamples = size(input,2)/SAhead;
    partitionX = floor(0.8*numSamples);
    partitionY = partitionX*SAhead;

    inputT = input(:,1:partitionY);
    outputT = output(:,1:partitionY);


    maxEpochs = 300;
    options = trainingOptions('adam', ...
    'ExecutionEnvironment','cpu', ...
    'MaxEpochs',maxEpochs, ...
    'OutputNetwork','last-iteration',...
    'ValidationFrequency',10,...
    'LearnRateSchedule','piecewise', ...
    'GradientDecayFactor',0.90,...
    'SquaredGradientDecayFactor',0.99,...
    'InitialLearnRate',0.00005,...
    'LearnRateDropFactor',0.9481, ...
    'LearnRateDropPeriod',24, ...
    'L2Regularization',0.00000000000002,...
    'MiniBatchSize',128,...
    'VerboseFrequency',1,...
    'Verbose',true);
    
    l1w = netOld.Layers(2, 1).Weights;
    l1b = netOld.Layers(2, 1).Bias;

    l2w = netOld.Layers(4, 1).Weights;
    l2b = netOld.Layers(4, 1).Bias;

    l3w = netOld.Layers(5, 1).Weights;
    l3b = netOld.Layers(5, 1).Bias;

    layers = [ ...
    sequenceInputLayer(4)
    ClosedLoopLayerStandard(l1w,l2w,l3w,l1b,l2b,l3b,SAhead)
    regressionLayer
    ];

    [net,info] = trainNetwork(inputT,outputT,layers,options); 
    save(['DPdnnStandardRetrain',num2str(snapsPerS),'Hz',num2str(max_t),'s','.mat'],'net','info','options','th1','th2','max_t','snapsPerS','layers');
    
end

function test_net_grid(double_penudlum,net,max_t,snapsPerS,th1,th2)

    timespan = 0:1/snapsPerS:max_t;

    err = 0;

    th1test = randi([th1(1)*10,th1(end)*10],1,1)
    th2test = randi([th2(1)*10,th2(end)*10],1,1)

    th1test = 17;
    th2test = 45;

    x0 = [pi/3*((th1test)/100),pi/3*((th2test)/100),0,0];

    [t,x] = ode45(double_penudlum,timespan ,x0(end,1:4)');

    x = x.';
    t = t.';
    x0 = x0.';

    ynn = zeros(4,length(timespan));
    ynn(:,1)=x0;

    for jj=2:(size(timespan,2)) 
        y0 = predict(net,x0);
        ynn(:,jj) = y0;
        x0 = y0;
    end
    err = x-ynn;
    err = err.^2;
    err = sum(err,2)/(size(err,2));
    err = sqrt(err);

    figure(1);
    cla
    print_subplot(1,'theta_{1}[rad]',err,x,ynn,t);
    print_subplot(2,'theta_{2}[rad]',err,x,ynn,t);
    print_subplot(3,'omega_{1}[rad/s]',err,x,ynn,t);
    print_subplot(4,'omega_{2}[rad/s]',err,x,ynn,t);
end

function test_net_grid_performance(double_penudlum,net,max_t,snapsPerS,th1,th2)
    th1test = randi([th1(1)*10,th1(end)*10],1,10);
    th2test = randi([th2(1)*10,th2(end)*10],1,10);
    errMat = zeros(length(th1test),length(th2test),4);
    timespan = 0:1/snapsPerS:max_t;
    errGlobal = 0;
    for i=1:length(th1test)
        for j=1:length(th2test)

            err = 0;
            x0 = [pi/3*((th1test(1,i))/100),pi/3*((th2test(1,j))/100),0,0];

            [t,x] = ode45(double_penudlum,timespan ,x0(end,1:4)');
        
            x = x.';
            t = t.';
            x0 = x0.';
            ynn = zeros(4,length(timespan));
            ynn(:,1)=x0;
        
            for jj=2:(size(timespan,2)) 
                y0 = predict(net,x0);
                ynn(:,jj) = y0;
                x0 = y0;
            end


            err = x-ynn;
            err = err.^2;
            err = sum(err,2)/(size(err,2));
            err = sqrt(err);
           
            errMat(i,j,:) = err;
        end
    end
    errGlobal = sum(sum(errMat,1),2);
    errGlobal = errGlobal/(length(th1test)*length(th2test));
    save('DPdnnStandardRetrainTest','errGlobal','th1test','th2test','errMat');
end

function print_subplot(index,name,err,x,ynn,t)
    sHandle = subplot(2,2,index);
    cla
    axis equal;box on, hold on;
    sHandle.XGrid = 'on';
    sHandle.YGrid = 'on';
    sHandle.YLabel.HorizontalAlignment = 'right';

    str = sprintf("RMSE = %s",err(index));
    title(str);

    plot(t(1:end),x(index,:),'b')
    plot(t(1:end),ynn(index,:),':','Linewidth',2,'Color','r')
    legend('ODE solver','NN');
    xlabel('$\emph{t} [s]$','Interpreter','latex');
    ylabel(['$\',name,'$'],'Interpreter','latex','Rotation',0);
end

function [input,output] = generate_data(th1,th2,max_t,snapsPerS,double_penudlum)

    timespan = 0:(1/snapsPerS):max_t;

    smpLength = size(timespan,2) - 1;
    numsamples = length(th1)*length(th2)-1;

    input = zeros(smpLength*numsamples,4);
    output = zeros(smpLength*numsamples,4);
    smpCounter = 1;

    for i=1:length(th1)
        for j=1:length(th2)
           
            x = [(pi/3)*(th1(i)/10),(pi/3)*(th2(j)/10),0,0];
            if (x == [0,0,0,0])
                
            else
                [t,x] = ode45(double_penudlum,timespan ,x(end,:)');

                offset = (smpCounter-1)*smpLength;
                input(offset+1:offset + smpLength,:) = x(1:end-1,:);
                output(offset+1:offset + smpLength,:) = x(2:end,:);
                smpCounter = smpCounter + 1;
            end
        end
    end
    input = input.';
    output = output.';
end

function dq = double_pendulum_system(x,l_1,l_2,m_1,m_2,g)
    theta1 = x(1);
    theta2 = x(2);
    omega1 = x(3);
    omega2 = x(4);

    dtheta1 = omega1;
    dtheta2 = omega2;
    domega1 = (g*m_2*sin(theta1 + 2*theta2) - g*m_2*sin(theta1) - 2*g*m_1*sin(theta1) + l_1*m_2*omega1^2*sin(2*theta2) + 2*l_2*m_2*omega1^2*sin(theta2) + 2*l_2*m_2*omega2^2*sin(theta2) + 4*l_2*m_2*omega1*omega2*sin(theta2))/(2*l_1*m_1 + l_1*m_2 - l_1*m_2*cos(2*theta2)); 
    domega2 = -(l_1^2*m_1*omega1^2*sin(theta2) + l_1^2*m_2*omega1^2*sin(theta2) + l_2^2*m_2*omega1^2*sin(theta2) + l_2^2*m_2*omega2^2*sin(theta2) - g*l_2*m_1*sin(theta1) - g*l_2*m_2*sin(theta1) + 2*l_2^2*m_2*omega1*omega2*sin(theta2) + g*l_1*m_1*cos(theta1)*sin(theta2) + g*l_1*m_2*cos(theta1)*sin(theta2) + l_1*l_2*m_2*omega1^2*sin(2*theta2) + (l_1*l_2*m_2*omega2^2*sin(2*theta2))/2 + g*l_2*m_2*cos(theta2)^2*sin(theta1) + g*l_2*m_2*cos(theta1)*cos(theta2)*sin(theta2) + l_1*l_2*m_2*omega1*omega2*sin(2*theta2))/(l_1*l_2*(m_1 + m_2 - m_2*cos(theta2)^2)); 

    dq = [dtheta1;dtheta2;domega1;domega2];
end

