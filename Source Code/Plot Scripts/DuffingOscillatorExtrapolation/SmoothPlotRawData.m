load('RawData.mat');

levels = 3000;
interpPts = 100;
numTests = 10;
diff = RawData(:,:,:,1,:,:) - RawData(:,:,:,2,:,:);
%% RMSE
RMSE = sqrt(sum((diff.^2),6)/size(RawData,6));
RMSE = sum(RMSE,3)/numTests;
RMSE = squeeze(RMSE);
%% RAE
RAE = sum(abs(diff),6)./sum(abs(RawData(:,:,:,1,:,:)),6);
RAE = sum(RAE,3)/numTests;
RAE = squeeze(RAE);


newpoints = interpPts;
omegaRes = 0.2750;
gammaRes = 0.1;
[xrow,ycol] = meshgrid(0:omegaRes:(6*1.5),0:gammaRes:0.5*3-0.01);
[xq,yq] = meshgrid(...
            linspace(min(min(xrow,[],2)),max(max(xrow,[],2)),newpoints ),...
            linspace(min(min(ycol,[],1)),max(max(ycol,[],1)),newpoints )...
          );

id = 2; % Set to 1 for displacement; 2 for velocity; Change c bar label accordingly

figure(2);
cla;
hold on;
RMSEExtraDataAvg = interp2(xrow,ycol,RMSE(:,:,id),xq,yq,'cubic');
[c,h]=contourf(xq,yq,RMSEExtraDataAvg,levels, 'edgecolor', 'none');
line([0.4 1.5 1.5 0.4 0.4],[0.1 0.1 0.5 0.5 0.1],'Color','k','LineWidth',2);
set(gca,'ColorScale','log')
colormap winter
cbar = colorbar;
xlabel('$\omega$ (rad/s)','Interpreter','latex');
ylabel('$\gamma$ (N)','Interpreter','latex');
xaxisproperties= get(gca, 'XAxis');
xaxisproperties.TickLabelInterpreter = 'latex';
yaxisproperties= get(gca, 'YAxis');
yaxisproperties.TickLabelInterpreter = 'latex';
colorbarproperties= get(gca, 'colorbar');
colorbarproperties.TickLabelInterpreter = 'latex';
ylabel(cbar,'RMSE Velocity (m/s)','Interpreter','latex');
%legend('Extrapolation','Training','Interpreter','latex');

