%yyaxis left
%ylim([-0.8, 0.8])
%ylabel('Velocity $v$ (m/s)','Interpreter','latex','FontSize',18)
%ax.YColor = 'black';
%yyaxis right
%ylim([-0.4, 0.4])
%ylabel('Forcing $F$ (N)','Interpreter','latex','FontSize',18)


figure(1);

subHnd = cell(6,1);
subData = cell(6,3);
for i=1:4
    subHnd{i} =  subplot(2,3,i);
    subData{i,1} = subHnd{i}.Children(1).YData;
    subData{i,2} = subHnd{i}.Children(2).YData;
    %subData{i,3} = subHnd{i}.Title.String;
end
s1 = subplot(2,3,1);
s2 = subplot(2,3,2);
s3 = subplot(2,3,3);
s4 = subplot(2,3,4);
s5 = subplot(2,3,5);
s6 = subplot(2,3,6);

xData = s1.Children.XData;
y1DataNN = s1.Children(1).YData;
y2DataNN = s2.Children(1).YData;
y3DataNN = s3.Children(1).YData;
y4DataNN = s4.Children(1).YData;
y5DataNN = s5.Children(1).YData;
y6DataNN = s6.Children(1).YData;

y1DataODE = s1.Children(2).YData;
y2DataODE = s2.Children(2).YData;
y3DataODE = s3.Children(2).YData;
y4DataODE = s4.Children(2).YData;
y5DataODE = s5.Children(2).YData;
y6DataODE = s6.Children(2).YData;

MAE1 = sum((abs(y1DataNN-y1DataODE)),2)/sum(abs(y1DataODE),2)*100;
MAE2 = sum((abs(y2DataNN-y2DataODE)),2)/sum(abs(y2DataODE),2)*100;
MAE3 = sum((abs(y3DataNN-y3DataODE)),2)/sum(abs(y3DataODE),2)*100;
MAE4 = sum((abs(y4DataNN-y4DataODE)),2)/sum(abs(y4DataODE),2)*100;
MAE5 = sum((abs(y5DataNN-y5DataODE)),2)/sum(abs(y5DataODE),2)*100;
MAE6 = sum((abs(y6DataNN-y6DataODE)),2)/sum(abs(y6DataODE),2)*100;

MAE1 = sprintf('%.2g', MAE1);
MAE2 = sprintf('%.2g', MAE2);
MAE3 = sprintf('%.2g', MAE3);
MAE4 = sprintf('%.2g', MAE4);
MAE5 = sprintf('%.2g', MAE5);
MAE6 = sprintf('%.2g', MAE6);

%%


f4 = figure(4);
%f4.WindowState = 'maximized';
f4.Position = [100 100 600 600];

t = tiledlayout(2,1);
t.TileSpacing = 'tight';
t.Padding = 'tight';

pause(1);
a = nexttile;
ax = gca;
hold on;

yyaxis left
plot(xData,y1DataNN,'Linewidth',2,'Color','#A2AD00');
plot(xData,y2DataNN,'-','Linewidth',2,'Color','#0065BD');
%legend(['$q_{1}$ NN (RAE: $',MAE1,'$\%)'],['$q_{2}$ NN (RAE: $',MAE2,'$\%)']);
ylabel('Angles $q_{1,2}$ (rad)','Interpreter','latex','FontSize',18);
ax.YColor = 'black';
yyaxis right
plot(xData,y3DataNN,'Linewidth',2,'Color','#E37222');
ax.YColor = 'black';
yyaxis left
plot(xData,y1DataODE,':','Linewidth',2,'Color','#808080');
plot(xData,y2DataODE,':','Linewidth',2,'Color','#808080');
yyaxis right
plot(xData,y3DataODE,':','Linewidth',2,'Color','#808080');
ylabel('Angle $q_{3}$ (rad)','Interpreter','latex','FontSize',18);


l3 = legend(['$q_{1}$ NN (RAE: $',MAE1,'$\%)'],['$q_{2}$ NN (RAE: $',MAE2,'$\%)'],['$q_{3}$ NN (RAE: $',MAE3,'$\%)'],'$q_{1},q_{2},q_{3}$ Reference','Location','northeast','FontSize',18,'Interpreter','latex');


%l1 = legend(stra,'Simscape','Location','northeast','FontSize',18,'Interpreter','latex');
%xlabel('$t$ [s]','Interpreter','latex','FontSize',18);

a.TickLabelInterpreter = 'latex';
a.XGrid = 'on';
a.YGrid = 'on';
a.Box = 'on';
a.GridColor = [0 0 0];
a.GridAlpha = 0.15;
set(a,'xticklabel',[])

%['$x$ DH NN (RAE: $',MAE1,'$\%)']
l3 = legend(['$q_{1}$ NN (RAE: $',MAE1,'$\%)'],['$q_{2}$ NN (RAE: $',MAE2,'$\%)'],['$q_{3}$ NN (RAE: $',MAE3,'$\%)'],'$q_{1},q_{2},q_{3}$ Reference','Location','northeast','FontSize',18,'Interpreter','latex');


c = nexttile;
hold on;

ax = gca;
yyaxis left
plot(xData,y4DataNN,'Linewidth',2,'Color','#A2AD00');
plot(xData,y5DataNN,'-','Linewidth',2,'Color','#0065BD');
ylabel('Angular Velocities $\omega_{1,2}$ (rad/s)','Interpreter','latex','FontSize',18);
ax.YColor = 'black';
yyaxis right
ax.YColor = 'black';
plot(xData,y6DataNN,'Linewidth',2,'Color','#E37222');
ylim([4.9,5.5])
yyaxis left
plot(xData,y4DataODE,':','Linewidth',2,'Color','#808080');
plot(xData,y5DataODE,':','Linewidth',2,'Color','#808080');
yyaxis right
plot(xData,y6DataODE,':','Linewidth',2,'Color','#808080');
ylabel('Angular Velocity $\omega_{3}$ (rad/s)','Interpreter','latex','FontSize',18);


xlabel('Time $t$ (s)','Interpreter','latex','FontSize',18);
%ylabel('Angular Velocities $\omega_{1,2}$ (rad/s)','Interpreter','latex','FontSize',18)
c.XGrid = 'on';
c.YGrid = 'on';
c.Box = 'on';
c.GridColor = [0 0 0];
c.GridAlpha = 0.15;
c.TickLabelInterpreter = 'latex';

hold on;
l3 = legend(['$\omega_{1}$ NN (RAE: $',MAE4,'$\%)'],['$\omega_{2}$ NN (RAE: $',MAE5,'$\%)'],['$\omega_{3}$ NN (RAE: $',MAE6,'$\%)'],'$\omega_{1},\omega_{2},\omega_{3}$ Reference','Location','northeast','FontSize',18,'Interpreter','latex');


linkaxes([a c],'x')
%export_fig MdlCheck2RAE.pdf -transparent -pdf 




