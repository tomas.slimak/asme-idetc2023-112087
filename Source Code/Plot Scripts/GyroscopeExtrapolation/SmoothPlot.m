load('ExtraDataAvg.mat');
newpoints = 300;

wy0Res = 0.5;
IzRes = 1;
[xrow,ycol] = meshgrid(0:wy0Res:(5-0.5),0:IzRes:7-1);
[xq,yq] = meshgrid(...
            linspace(min(min(xrow,[],2)),max(max(xrow,[],2)),newpoints ),...
            linspace(min(min(ycol,[],1)),max(max(ycol,[],1)),newpoints )...
          );

id = 1;

figure(2);
cla;
hold on;
RMSEExtraDataAvg = RMSEExtraDataAvg(:,:,id);
RMSEExtraDataAvg = RMSEExtraDataAvg.';
RMSEExtraDataAvg = interp2(xrow,ycol,RMSEExtraDataAvg,xq,yq,'cubic');
[c,h]=contourf(xq,yq,RMSEExtraDataAvg,1000, 'edgecolor', 'none');
line([1 1.5 1.5 1 1],[2 2 4 4 2],'Color','k','LineWidth',2);
set(gca,'ColorScale','log')
colormap winter
cbar = colorbar;
xlabel('$\omega_{y0}$ (rad/s)','Interpreter','latex');
ylabel('$I_z$ (kg m$^2$)','Interpreter','latex');
xaxisproperties= get(gca, 'XAxis');
xaxisproperties.TickLabelInterpreter = 'latex';
yaxisproperties= get(gca, 'YAxis');
yaxisproperties.TickLabelInterpreter = 'latex';
colorbarproperties= get(gca, 'colorbar');
colorbarproperties.TickLabelInterpreter = 'latex';
ylabel(cbar,'RMSE $q_1$ (rad/s)','Interpreter','latex');


