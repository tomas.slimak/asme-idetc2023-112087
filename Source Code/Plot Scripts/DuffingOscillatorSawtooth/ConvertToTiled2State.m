figure(1);

subHnd = cell(2,1);
subData = cell(2,3);

for i=1:2
    subHnd{i} =  subplot(2,1,i);
    subData{i,1} = subHnd{i}.Children(1).YData;
    subData{i,2} = subHnd{i}.Children(2).YData;
    %subData{i,3} = subHnd{i}.Title.String;
end
s1 = subplot(2,1,1);
s2 = subplot(2,1,2);

xData = s1.Children.XData;
y1DataNN = s1.Children(1).YData;
y2DataNN = s2.Children(1).YData;

y1DataODE = s1.Children(2).YData;
y2DataODE = s2.Children(2).YData;

MAE1 = sum((abs(y1DataNN-y1DataODE)),2)/sum(abs(y1DataODE),2)*100;
MAE2 = sum((abs(y2DataNN-y2DataODE)),2)/sum(abs(y2DataODE),2)*100;
MAE1 = sprintf('%.2g', MAE1);
MAE2 = sprintf('%.2g', MAE2);

%MAE1 = MAE1/(10^110);
%MAE2 = MAE2/(10^110);
%MAE3 = MAE3/(10^110);
%MAE4 = MAE4/(10^110);

%stra = ['NN (MAE = ',num2str(MAE1*100),' \\%% )'];

%stra = sprintf('DMD (RAE = $%.2f \\times 10^{110}$\\%%)',MAE1*100);
%strb = sprintf('DMD (RAE = $%.2f \\times 10^{110}$\\%%)',MAE2*100);
%strc = sprintf('DMD (RAE = $%.2f \\times 10^{110}$\\%%)',MAE3*100);
%strd = sprintf('DMD (RAE = $%.2f \\times 10^{110}$\\%%)',MAE4*100);

%stra = sprintf('ODE Solver(RAE = $%.2g$\\%%)',MAE1*100);
%strb = sprintf('ODE Solver(RAE = $%.2g$\\%%)',MAE2*100);

%stra = sprintf('Noise ($%.2f$\\%%)',2);
%strb = sprintf('Noise ($%.2f$\\%%)',2);
%strc = sprintf('Noise ($%.2f$\\%%)',2);
%strd = sprintf('Noise ($%.2f$\\%%)',2);

%f2 = figure(2);
%f2.WindowState = 'maximized';
%
%t = tiledlayout(2,1);
%t.TileSpacing = 'tight';
%t.Padding = 'tight';
%
%pause(1);
%a = nexttile;
%hold on;
%plot(xData,y1DataNN,':','Linewidth',2,'Color','#A2AD00');
%plot(xData,y1DataODE,'Linewidth',2,'Color','#A2AD00');
%l1 = legend(stra,'Simscape','Location','northeast','FontSize',18,'Interpreter','latex');
%xlabel('$t$ [s]','Interpreter','latex','FontSize',18);
%ylabel('$\theta_{1}$ [rad]','Interpreter','latex','FontSize',18);
%a.TickLabelInterpreter = 'latex';
%a.XGrid = 'on';
%a.YGrid = 'on';
%a.Box = 'on';
%a.GridColor = [0 0 0];
%a.GridAlpha = 0.45;
%
%
%b = nexttile;
%hold on;
%plot(xData,y2DataNN,':','Linewidth',2,'Color','#E37222');
%plot(xData,y2DataODE,'Linewidth',2,'Color','#E37222');
%l2 = legend(strb,'Simscape','Location','northeast','FontSize',18,'Interpreter','latex');
%xlabel('$t$ [s]','Interpreter','latex','FontSize',18);
%ylabel('$\theta_{2}$ [rad]','Interpreter','latex','FontSize',18)
%b.XGrid = 'on';
%b.YGrid = 'on';
%b.Box = 'on';
%b.GridColor = [0 0 0];
%b.GridAlpha = 0.45;
%b.TickLabelInterpreter = 'latex';

%a1 = annotation('textbox','String',stra,'Position',a.Position,'FitBoxToText','on','Interpreter','latex','FontSize',11,EdgeColor = [0.15,0.15,0.15]);
%a2 = annotation('textbox','String',strb,'Position',b.Position,'FitBoxToText','on','Interpreter','latex','FontSize',11,EdgeColor = [0.15,0.15,0.15]);
%a3 = annotation('textbox','String',strc,'Position',c.Position,'FitBoxToText','on','Interpreter','latex','FontSize',11,EdgeColor = [0.15,0.15,0.15]);
%a4 = annotation('textbox','String',strd,'Position',d.Position,'FitBoxToText','on','Interpreter','latex','FontSize',11,EdgeColor = [0.15,0.15,0.15]);


%a1.Position(2) = a1.Position(2) + a1.Position(4);
%a2.Position(2) = a2.Position(2) + a2.Position(4);
%a3.Position(2) = a3.Position(2) + a3.Position(4);
%a4.Position(2) = a4.Position(2) + a4.Position(4);

%linkaxes([a b],'xy')
%linkaxes([c d],'xy')


%export_fig MdlCheck2RAE.pdf -transparent -pdf 



%f3 = figure(3);
%f3.WindowState = 'maximized';
%
%pause(1);
%hold on;
%plot(xData,y1DataNN,':','Linewidth',2,'Color','#A2AD00');
%plot(xData,y1DataODE,'Linewidth',2,'Color','#A2AD00');
%
%
%hold on;
%plot(xData,y2DataNN,':','Linewidth',2,'Color','#E37222');
%plot(xData,y2DataODE,'Linewidth',2,'Color','#E37222');
%
%
%%hold on;
%%plot(xData,y3DataNN,':','Linewidth',2,'Color','#98C6EA');
%%plot(xData,y3DataODE,'Linewidth',2,'Color','#98C6EA');
%%
%%
%%hold on;
%%plot(xData,y4DataNN,':','Linewidth',2,'Color','#0065BD');
%%plot(xData,y4DataODE,'Linewidth',2,'Color','#0065BD');
%
%
%l4 = legend(strd,'Simscape','Location','northeast','FontSize',18,'Interpreter','latex');
%xlabel('$t$ [s]','Interpreter','latex','FontSize',18);
%ylabel('$\omega_{2}$ [rad/s]','Interpreter','latex','FontSize',18)
%XGrid = 'on';
%YGrid = 'on';
%Box = 'on';
%GridColor = [0 0 0];
%GridAlpha = 0.45;
%TickLabelInterpreter = 'latex';


%%

load('SawtoothProfile0_9radPerS.mat','sawtooth')

f4 = figure(4);
%f4.WindowState = 'maximized';
f4.Position = [100 100 600 600];

t = tiledlayout(2,1);
t.TileSpacing = 'tight';
t.Padding = 'tight';

pause(1);
a = nexttile;
hold on;
plot(xData,y1DataNN,'Linewidth',2,'Color','#A2AD00');
plot(xData,y1DataODE,':','Linewidth',2,'Color','#A2AD00');
%l1 = legend(stra,'Simscape','Location','northeast','FontSize',18,'Interpreter','latex');
%xlabel('$t$ [s]','Interpreter','latex','FontSize',18);
ylabel('Position $x$ (m)','Interpreter','latex','FontSize',18);
a.TickLabelInterpreter = 'latex';
a.XGrid = 'on';
a.YGrid = 'on';
a.Box = 'on';
a.GridColor = [0 0 0];
a.GridAlpha = 0.15;
set(a,'xticklabel',[])


hold on;
%plot(xData,y2DataNN,'Linewidth',2,'Color','#E37222');
%plot(xData,y2DataODE,':','Linewidth',2,'Color','#E37222');
l3 = legend(['$x$ NN (RAE: $',MAE1,'$\%)'],'$x$ Reference','Location','northeast','FontSize',18,'Interpreter','latex');
%stra = ['NN (MAE = ',num2str(MAE1*100),' \\%% )'];

c = nexttile;
ax = gca;
hold on;
yyaxis left
plot(xData,y2DataNN,'Linewidth',2,'Color','#0065BD');
plot(xData,y2DataODE,':','Linewidth',2,'Color','#0065BD');
yyaxis right;
plot(xData,sawtooth,':','Linewidth',2,'Color','#808080');
xlabel('Time $t$ (s)','Interpreter','latex','FontSize',18);
yyaxis left
ylim([-0.8, 0.8])
ylabel('Velocity $v$ (m/s)','Interpreter','latex','FontSize',18)
ax.YColor = 'black';
yyaxis right
ylim([-0.4, 0.4])
ylabel('Forcing $F$ (N)','Interpreter','latex','FontSize',18)
ax.YColor = 'black';
c.XGrid = 'on';
c.YGrid = 'on';
c.Box = 'on';
c.GridColor = [0 0 0];
c.GridAlpha = 0.15;
c.TickLabelInterpreter = 'latex';

hold on;
%plot(xData,y4DataNN,'Linewidth',2,'Color','#0065BD');
%plot(xData,y4DataODE,':','Linewidth',2,'Color','#0065BD');
l3 = legend(['$v$ NN (RAE: $',MAE2,'$\%)'],'$v$ Reference','$F$ Forcing','Location','northeast','FontSize',18,'Interpreter','latex');


linkaxes([a c],'x')
export_fig MdlCheck2RAE.pdf -transparent -pdf 




