# Description

Folder contains following subfolders:

* Folder containing refined loading extrapolation results
* Models required for data acquisition for NN training and NN results comparison
* NN Data including NNs themselves and appropriate tests outlined in the paper
* Raw format of plots presented in the paper
* Scripts required for NN training
* Specific training scripts required for NN training