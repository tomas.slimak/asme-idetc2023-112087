# Description

Folder contains following scripts and data:

* Simulink model for data acquisition for Duffing oscillator under single harmonic and sawtooth load
* Simuling model for data acquisition for Duffing oscillator under double harmonic load