# Description

Folder contains following scripts and data:

* Script required to train Duffing oscillator NN with either single harmonic or sawtooth load
* Script required to train Duffing oscillator NN with double harmonic load