smpFreq = 100;
max_t = 50;
model = 'DuffingOscillatorDouble';

x0 = 0;
v0 = 0;

m = 1;
l = 2;

alpha = -1;
beta = 1;
delta = 0.3;


init_SimScape_Model(model,smpFreq,max_t);

%get_single_trajectory(model,x0,v0,m,l,alpha,beta,gamma,delta,omega);

%train_net_grid(model,max_t,smpFreq,m,l,alpha,beta,delta)

load('DOForcedLSTMAcc20s.mat');

max_t = 50;

test_net(model,net,x0,v0,m,l,alpha,beta,gamma,delta,omega,gamma,omega,max_t,smpFreq);

%test_net_performance(model,net,x0,v0,m,l,alpha,beta,gamma,delta,omega,max_t,smpFreq)

function train_net_grid(model,max_t,smpFreq,m,l,alpha,beta,delta)

    x0 = 0;
    v0 = 0;

    gamma = 0.7*(0.1:0.2:0.5); %0.1  -   0.5
    omega = 0.4:0.36:1.5; % 0.4   -  1.5

    gamma2 =  0.7*(0.1:0.2:0.5); %0.1  -   0.5
    omega2 = 0.4:0.36:1.5; % 0.4   -  1.5

    [input,output] = get_multiple_trajectories(model,x0,v0,m,l,alpha,beta,gamma,delta,omega,gamma2,omega2,max_t,smpFreq);

    partition = floor(0.8*size(input,2));

    inputT = input(:,1:partition-1);
    outputT = output(:,1:partition-1);

    inputV = input(:,partition:end);
    outputV = output(:,partition:end);

    maxEpochs = 200;
    options = trainingOptions('adam', ...
    'ExecutionEnvironment','gpu', ...
    'MaxEpochs',maxEpochs, ...
    'OutputNetwork','best-validation-loss',...
    'ValidationFrequency',10, ...
    'Shuffle','every-epoch', ...
    'LearnRateSchedule','piecewise', ...
    'GradientDecayFactor',0.90,...
    'SquaredGradientDecayFactor',0.99,...
    'InitialLearnRate',0.0074,... %0.0074
    'LearnRateDropFactor',0.9606, ... %0.9606
    'LearnRateDropPeriod',37, ... %37
    'L2Regularization',0.2,...
    'MiniBatchSize',128,...
    'ValidationData',{inputV,outputV});
    

    layers = [ ...
    sequenceInputLayer(3)
    lstmLayer(36)
    fullyConnectedLayer(83)
    leakyReluLayer(0.1)
    fullyConnectedLayer(46)
    fullyConnectedLayer(1)
    regressionLayer
    ];

    [net,info] = trainNetwork(inputT,outputT,layers,options); 
    save('DOForcedLSTMAcc20sDouble.mat','net','info','options','gamma','omega','gamma2','omega2','max_t','smpFreq','layers');
    
end

function test_net(model,net,x0,v0,m,l,alpha,beta,gamma,delta,omega,gamma2,omega2,max_t,smpFreq)
    timespan = 0:1/smpFreq:max_t;

    gammaTest = randi([gamma(1)*100,gamma(end)*100],1,1)/100*0.7
    omegaTest = randi([omega(1)*100,omega(end)*100],1,1)/100

    gammaTest2 = randi([gamma2(1)*100,gamma2(end)*100],1,1)/100*0.7
    omegaTest2 = randi([omega2(1)*100,omega2(end)*100],1,1)/100

    %gammaTest = 0.6;
    %omegaTest = 1.2;

    x = get_single_trajectory(model,x0,v0,m,l,alpha,beta,gammaTest,delta,omegaTest,gammaTest2,omegaTest2);

    x0 = [x(1:2,1);x(3,1)];

    ynn = zeros(2,length(timespan));
    ynn(:,1)=x0(1:2);

    net = resetState(net);    

    for jj=2:(size(timespan,2)) 
        %y0 = predict(net,x0);
        [net,y0] = predictAndUpdateState(net,x0);
        xadv_v = x0(2,1) + y0*(1/smpFreq);
        xadv_x = x0(1,1) + (xadv_v)*(1/smpFreq);
        x0 = [xadv_x;xadv_v;x(4,jj)];
        ynn(:,jj) = x0(1:2);
    end

    err = x(1:2,:)-ynn;
    err = err.^2;
    err = sum(err,2)/(size(err,2));
    err = sqrt(err);

    figure(1);
    cla
    print_subplot(1,'x [m]',err,x,ynn,timespan);
    print_subplot(2,'v [m/s]',err,x,ynn,timespan);

end

function test_net_performance(model,net,x0,v0,m,l,alpha,beta,gamma,delta,omega,max_t,smpFreq)
    timespan = 0:1/smpFreq:max_t;

    gammaTest = randi([gamma(1)*100,gamma(end)*100],1,10)/100*0.7;
    omegaTest = randi([omega(1)*100,omega(end)*100],1,10)/100;

    gammaTest2 = randi([gamma(1)*100,gamma(end)*100],1,10)/100*0.7;
    omegaTest2 = randi([omega(1)*100,omega(end)*100],1,10)/100;

    errMat = zeros(size(gammaTest,2),size(omegaTest,2),2);

    for i=1:size(gammaTest,2)
        for j=1:size(omegaTest,2)
            x0 = 0;
            v0 = 0;

            x = get_single_trajectory(model,x0,v0,m,l,alpha,beta,gammaTest(1,i),delta,omegaTest(1,j),gammaTest2(1,i),omegaTest2(1,j));

            x0 = [x(1:2,1);x(3,1)];
        
            ynn = zeros(2,length(timespan));
            ynn(:,1)=x0(1:2);

            net = resetState(net);   
        
            for jj=2:(size(timespan,2)) 
                %y0 = predict(net,x0);
                [net,y0] = predictAndUpdateState(net,x0);
                xadv_v = x0(2,1) + y0*(1/smpFreq);
                xadv_x = x0(1,1) + (xadv_v)*(1/smpFreq);
                x0 = [xadv_x;xadv_v;x(4,jj)];
                ynn(:,jj) = x0(1:2);
            end
        
            err = x(1:2,:)-ynn;
            err = err.^2;
            err = sum(err,2)/(size(err,2));
            err = sqrt(err);

            errMat(i,j,:) = err;

            figure(1);
            cla
            print_subplot(1,'x [m]',err,x,ynn,timespan);
            print_subplot(2,'v [m/s]',err,x,ynn,timespan);

        end
    end
    errAvg = sum(sum(errMat,1),2)/(size(omegaTest,2)*size(gammaTest,2))
    save('DOForcedLSTMAcc50s_Test_50s.mat','errMat','errAvg','net');
end

function print_subplot(index,name,err,x,ynn,t)
    sHandle = subplot(2,1,index);
    cla
    box on, hold on;
    sHandle.XGrid = 'on';
    sHandle.YGrid = 'on';
    sHandle.YLabel.HorizontalAlignment = 'right';

    str = sprintf("RMSE = %s",err(index));
    title(str);

    plot(t(1:end),x(index,:),'b')
    plot(t(1:end),ynn(index,:),':','Linewidth',2,'Color','r')
    legend('ODE solver','NN');
    xlabel('$\emph{t} [s]$','Interpreter','latex');
    ylabel(['$',name,'$'],'Interpreter','latex','Rotation',0);
end

function [input,output] = get_multiple_trajectories(model,x0,v0,m,l,alpha,beta,gamma,delta,omega,gamma2,omega2,max_t,smpFreq)
    trajLength = max_t*smpFreq+1;

    input = zeros(3,trajLength*(size(gamma,2)*size(omega,2)-1));
    output = zeros(1,trajLength*(size(gamma,2)*size(omega,2)-1));
    offset = 0;
    for i=1:size(gamma,2)
        for j=1:size(omega,2)
            for k=1:size(gamma2,2)
                for l=1:size(omega2,2)
                    if(gamma(i) == 0 && omega(j) == 0)
                        a = 5;
                    else
                        trajectory = get_single_trajectory(model,x0,v0,m,l,alpha,beta,gamma(1,i),delta,omega(1,j),gamma2(1,k),omega2(1,l));
        
                        input(1:2,1+offset:trajLength + offset) = trajectory(1:2,1:end);
                        input(3,1+offset:trajLength + offset) = trajectory(4,1:end);
        
                        output(:,1+offset:trajLength + offset) = trajectory(3,1:end);
                        offset = offset + trajLength;
                    end
                end
            end
        end
    end
end

function init_SimScape_Model(model,smpFreq,max_t)
    set_param(model,'FastRestart','off');
    set_param([model , '/Prismatic Joint'], 'PositionTargetValue_conf', 'runtime')
    set_param([model , '/Prismatic Joint'], 'VelocityTargetValue_conf', 'runtime')
    
    set_param([model , '/Brick Solid'], 'BrickDimensions_conf', 'runtime')
    set_param([model , '/Brick Solid'], 'Mass_conf', 'runtime')

    set_param([model , '/Gain'], 'Gain', 'runtime')
    set_param([model , '/Gain1'], 'Gain', 'runtime')
    set_param([model , '/Gain2'], 'Gain', 'runtime')
    set_param([model , '/Gain3'], 'Gain', 'runtime')
    set_param([model , '/Gain4'], 'Gain', 'runtime')
    set_param([model , '/Gain5'], 'Gain', 'runtime')
    set_param([model , '/Gain6'], 'Gain', 'runtime')


    set_param(model,'FixedStep',num2str(1/smpFreq));
    set_param(model,'StopTime', num2str(max_t));
    set_param(model,'FastRestart','on');
end

function trajectory = get_single_trajectory(model,x0,v0,m,l,alpha,beta,gamma,delta,omega,gamma2,omega2)
        set_param('DuffingOscillatorDouble/Prismatic Joint','PositionTargetValue',num2str(x0));
        set_param('DuffingOscillatorDouble/Prismatic Joint','VelocityTargetValue',num2str(v0));
        
        %set_param('DuffingOscilator/Brick Solid','BrickDimensions',['[',num2str([l,0.1,0.1]),']']);
        set_param('DuffingOscillatorDouble/Brick Solid','Mass',num2str(m));

        set_param('DuffingOscillatorDouble/Gain','Gain',num2str(-delta));
        set_param('DuffingOscillatorDouble/Gain1','Gain',num2str(-alpha));
        set_param('DuffingOscillatorDouble/Gain2','Gain',num2str(-beta));
        set_param('DuffingOscillatorDouble/Gain3','Gain',num2str(omega));
        set_param('DuffingOscillatorDouble/Gain4','Gain',num2str(-gamma));
        set_param('DuffingOscillatorDouble/Gain5','Gain',num2str(omega2));
        set_param('DuffingOscillatorDouble/Gain6','Gain',num2str(-gamma2));

        simulation=sim(model);
        results=simulation.simout;
        trajectory = results(1).Data;
        trajectory = trajectory.';
end