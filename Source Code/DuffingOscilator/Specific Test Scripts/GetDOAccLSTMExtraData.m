smpFreq = 100;
max_t = 50;
model = 'DuffingOscilatorAcc';

x0 = 0;
v0 = 0;

m = 1;
l = 2;

alpha = -1;
beta = 1;
delta = 0.3;

init_SimScape_Model(model,smpFreq,max_t);

load('DOForcedLSTMAcc20s.mat');

max_t = 50;

%test_net(model,net,x0,v0,m,l,alpha,beta,gamma,delta,omega,max_t,smpFreq);

gather_extra_data(model,net,x0,v0,m,l,alpha,beta,gamma,delta,omega,max_t,smpFreq)

function test_net(model,net,x0,v0,m,l,alpha,beta,gamma,delta,omega,max_t,smpFreq)
    timespan = 0:1/smpFreq:max_t;

    gammaTest = randi([int32(gamma(1)*100),int32(gamma(end)*100)],1,1)/100
    omegaTest = randi([int32(omega(1)*100),int32(omega(end)*100)],1,1)/100

    gammaTest = 1.2;
    omegaTest = 8;

    x = get_single_trajectory(model,x0,v0,m,l,alpha,beta,gammaTest,delta,omegaTest);

    x0 = [x(1:2,1);x(3,1)];

    ynn = zeros(2,length(timespan));
    ynn(:,1)=x0(1:2);

    net = resetState(net);
    
    for jj=2:(size(timespan,2)) 
        [net,y0] = predictAndUpdateState(net,x0);
        xadv_v = x0(2,1) + y0*(1/smpFreq);
        xadv_x = x0(1,1) + x0(2,1)*(1/smpFreq);
        x0 = [xadv_x;xadv_v;x(4,jj)];
        ynn(:,jj) = x0(1:2);
    end

    err = x(1:2,:)-ynn;
    err = err.^2;
    err = sum(err,2)/(size(err,2));
    err = sqrt(err);

    figure(1);
    cla
    print_subplot(1,'x [m]',err,x,ynn,timespan);
    print_subplot(2,'v [m/s]',err,x,ynn,timespan);
end

function [x,ynn] = get_single_run(model,net,x0,v0,m,l,alpha,beta,gamma,delta,omega,max_t,smpFreq)
    timespan = 0:1/smpFreq:max_t;

    x = get_single_trajectory(model,x0,v0,m,l,alpha,beta,gamma,delta,omega);

    x0 = [x(1:2,1);x(3,1)];

    ynn = zeros(2,length(timespan));
    ynn(:,1)=x0(1:2);

    net = resetState(net);

    for jj=2:(size(timespan,2)) 
        [net,y0] = predictAndUpdateState(net,x0);
        %xadv_v = x0(2,1) + y0*(1/smpFreq);
        %xadv_x = x0(1,1) + (xadv_v)*(1/smpFreq);
        xadv_v = x0(2,1) + y0*(1/smpFreq);
        xadv_x = x0(1,1) + x0(2,1)*(1/smpFreq);
        x0 = [xadv_x;xadv_v;x(4,jj)];
        ynn(:,jj) = x0(1:2);
    end
end

function gather_extra_data(model,net,x0,v0,m,l,alpha,beta,gamma,delta,omega,max_t,smpFreq)
    gammaTr = [gamma(1,1),gamma(1,end)];
    omegaTr = [omega(1,1),omega(1,end)];

    trainResGamma = 4;
    trainResOmega = 4;
    gammaRes = (gammaTr(1,2)-gammaTr(1,1))/trainResGamma;
    omegaRes = (omegaTr(1,2)-omegaTr(1,1))/trainResOmega;

    gammaMin = 0;
    gammaMax = 1.6*gammaTr(1,2);

    omegaMin = 0;
    omegaMax = 4*omegaTr(1,2);

    omegaTest = omegaMin:omegaRes:(omegaMax+0.06);
    gammaTest = gammaMin:gammaRes:(gammaMax+0.01);

    numTests = 2;

    testParameters = zeros(numel(gammaTest)-1,numel(omegaTest)-1,numTests,2);
    RawData = zeros(numel(gammaTest)-1,size(testParameters,2),numTests,2,2,(max_t*smpFreq+1));

    for i=1:numel(gammaTest)-1
        for j=1:numel(omegaTest)-1
            gammaMin = gammaTest(1,i);
            gammaMax = gammaTest(1,i+1);
            omegaMin = omegaTest(1,j);
            omegaMax = omegaTest(1,j+1);

            gamma = randi([int32(gammaMin*100),int32(gammaMax*100)],1,numTests)/100;
            omega = randi([int32(omegaMin*100),int32(omegaMax*100)],1,numTests)/100;
            testParameters(i,j,:,1) = gamma;
            testParameters(i,j,:,2) = omega;
            for k=1:numTests
                [x,ynn] = get_single_run(model,net,x0,v0,m,l,alpha,beta,gamma(1,1,k),delta,omega(1,1,k),max_t,smpFreq);
                RawData(i,j,k,1,:,:) = x(1:2,:);
                RawData(i,j,k,2,:,:) = ynn;
            end 
        end
    end
    save('RawData.mat','RawData','testParameters');
    save('testParameters.mat','testParameters');
end

function print_subplot(index,name,err,x,ynn,t)
    sHandle = subplot(2,1,index);
    cla
    box on, hold on;
    sHandle.XGrid = 'on';
    sHandle.YGrid = 'on';
    sHandle.YLabel.HorizontalAlignment = 'right';

    str = sprintf("RMSE = %s",err(index));
    title(str);

    plot(t(1:end),x(index,:),'b')
    plot(t(1:end),ynn(index,:),':','Linewidth',2,'Color','r')
    legend('ODE solver','NN');
    xlabel('$\emph{t} [s]$','Interpreter','latex');
    ylabel(['$',name,'$'],'Interpreter','latex','Rotation',0);
end

function init_SimScape_Model(model,smpFreq,max_t)
    set_param(model,'FastRestart','off');
    set_param([model , '/Prismatic Joint'], 'PositionTargetValue_conf', 'runtime')
    set_param([model , '/Prismatic Joint'], 'VelocityTargetValue_conf', 'runtime')
    
    set_param([model , '/Brick Solid'], 'BrickDimensions_conf', 'runtime')
    set_param([model , '/Brick Solid'], 'Mass_conf', 'runtime')

    set_param([model , '/Gain'], 'Gain', 'runtime')
    set_param([model , '/Gain1'], 'Gain', 'runtime')
    set_param([model , '/Gain2'], 'Gain', 'runtime')
    set_param([model , '/Gain3'], 'Gain', 'runtime')
    set_param([model , '/Gain4'], 'Gain', 'runtime')


    set_param(model,'FixedStep',num2str(1/smpFreq));
    set_param(model,'StopTime', num2str(max_t));
    set_param(model,'FastRestart','on');
end

function trajectory = get_single_trajectory(model,x0,v0,m,l,alpha,beta,gamma,delta,omega)
        set_param('DuffingOscilatorAcc/Prismatic Joint','PositionTargetValue',num2str(x0));
        set_param('DuffingOscilatorAcc/Prismatic Joint','VelocityTargetValue',num2str(v0));
        
        set_param('DuffingOscilatorAcc/Brick Solid','Mass',num2str(m));

        set_param('DuffingOscilatorAcc/Gain','Gain',num2str(-delta));
        set_param('DuffingOscilatorAcc/Gain1','Gain',num2str(-alpha));
        set_param('DuffingOscilatorAcc/Gain2','Gain',num2str(-beta));
        set_param('DuffingOscilatorAcc/Gain3','Gain',num2str(omega));
        set_param('DuffingOscilatorAcc/Gain4','Gain',num2str(-gamma));

        simulation=sim(model);
        results=simulation.simout;
        trajectory = results(1).Data;
        trajectory = trajectory.';
end