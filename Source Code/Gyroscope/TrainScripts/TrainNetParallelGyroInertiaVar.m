%% PARALLEL NN WITH INERTIA VARIATION

smpFreq = 100;
max_t = 1;
model = 'Gyroscope';

wx0 = 0;
wy0 = 1;
wz0 = 5;

init_SimScape_Model(model,smpFreq,max_t);
%trajectory = get_single_trajectory(model,wx0,wy0,wz0);

%train_net_grid(max_t,smpFreq,model)

load('GyroParallelInertiaVar.mat');

test_net(model,nets,wy0,wz0,Iz,max_t,smpFreq)

%test_net_performance(model,nets,wy0,wz0,Iz,max_t,smpFreq)

function train_net_grid(max_t,smpFreq,model)
    wx0 = 0;

    wy0 = 1:0.5:2; 
    wz0 = 5:1:7;
    Iz = 2:1:5;

    nets = cell(6,2);

    [input,output] = get_multiple_trajectories(model,wx0,wy0,wz0,Iz,max_t,smpFreq);

    partition = floor(0.8*size(input,2));

    inputT = input(:,1:partition-1);
    outputT = output(:,1:partition-1);

    inputV = input(:,partition:end);
    outputV = output(:,partition:end);

    maxEpochs = 6000;
    options = trainingOptions('adam', ...
    'ExecutionEnvironment','gpu', ...
    'MaxEpochs',maxEpochs, ...
    'OutputNetwork','best-validation-loss',...
    'ValidationFrequency',10, ...
    'Shuffle','every-epoch', ...
    'LearnRateSchedule','piecewise', ...
    'GradientDecayFactor',0.90,...
    'SquaredGradientDecayFactor',0.99,...
    'InitialLearnRate',0.0474,...
    'LearnRateDropFactor',0.9606, ...
    'LearnRateDropPeriod',37, ...
    'L2Regularization',0.2,...
    'MiniBatchSize',128,...
    'ValidationData',{inputV,outputV});

    layers = [ ...
    sequenceInputLayer(7)
    fullyConnectedLayer(26)
    leakyReluLayer(0.1)
    fullyConnectedLayer(17)
    fullyConnectedLayer(1)
    regressionLayer
    ];

    layers1 = [ ...
    sequenceInputLayer(7)
    fullyConnectedLayer(8)
    functionLayer(@(x) sin(x))
    fullyConnectedLayer(4)
    fullyConnectedLayer(1)
    regressionLayer
    ];

    layers2 = [ ...
    sequenceInputLayer(7)
    fullyConnectedLayer(8)
    functionLayer(@(x) sin(x))
    fullyConnectedLayer(4)
    fullyConnectedLayer(1)
    regressionLayer
    ];
    
    layers6 = [ ...
    sequenceInputLayer(7)
    fullyConnectedLayer(26)
    reluLayer
    fullyConnectedLayer(17)
    fullyConnectedLayer(1)
    regressionLayer
    ];

    layersC = cell(6,1);
    layersC{1,1} = layers1;
    layersC{2,1} = layers2;
    layersC{3,1} = layers;
    layersC{4,1} = layers;
    layersC{5,1} = layers;
    layersC{6,1} = layers6;

    for i=1:6
        options.ValidationData = {inputV,outputV(i,:)};
        [net,info] = trainNetwork(inputT,outputT(i,:),layersC{i,1},options); 
        nets{i,1} = net;
        nets{i,2} = info;
    end
    save('GyroPSSHyper2ProperInertiaVarSmaller2s250Hz.mat','nets','options','max_t','smpFreq','layersC','wx0','wy0','wz0','Iz');

end

function test_net(model,nets,wy0,wz0,Iz,max_t,smpFreq)
    timespan = 0:1/smpFreq:max_t;

    wx0 = 0;

    wy0Test = randi([wy0(1)*100,wy0(end)*100],1,1)/100
    wz0Test = randi([wz0(1)*100,wz0(end)*100],1,1)/100
    IzTest = randi([Iz(1)*100,Iz(end)*100],1,1)/100

    x = get_single_trajectory(model,wx0,wy0Test,wz0Test,IzTest);

    x0 = [x(1:6,1);IzTest];

    ynn = zeros(6,length(timespan));
    ynn(:,1)=x(1:6,1);

    y0 = zeros(6,1);

    for jj=2:(size(timespan,2))
        for k = 1:6
            y0(k,1) = predict(nets{k,1},x0);
        end
        ynn(:,jj) = y0;
        x0 = [y0;IzTest];
    end

    err = x(1:6,:)-ynn;
    err = err.^2;
    err = sum(err,2)/(size(err,2));
    err = sqrt(err);

    figure(1);
    cla
    print_subplot(1,'q_{1} [rad]',err,x,ynn,timespan);
    print_subplot(2,'q_{2} [rad]',err,x,ynn,timespan);
    print_subplot(3,'q_{3} [rad]',err,x,ynn,timespan);
    print_subplot(4,'\omega_{1} [rad/s]',err,x,ynn,timespan);
    print_subplot(5,'\omega_{2} [rad/s]',err,x,ynn,timespan);
    print_subplot(6,'\omega_{3} [rad/s]',err,x,ynn,timespan);

end

function test_net_performance(model,nets,wy0,wz0,Iz,max_t,smpFreq)
    timespan = 0:1/smpFreq:max_t;

    wx0 = 0;

    wy0Test = randi([wy0(1)*100,wy0(end)*100],1,5)/100;
    wz0Test = randi([wz0(1)*100,wz0(end)*100],1,5)/100;
    IzTest = randi([Iz(1)*100,Iz(end)*100],1,4)/100;

    errMat = zeros(size(wy0Test,2),size(wz0Test,2),size(IzTest,2),6);

    for i=1:size(wy0Test,2)
        for j=1:size(wz0Test,2)
            for k=1:size(IzTest,2)
                x = get_single_trajectory(model,wx0,wy0Test(1,i),wz0Test(1,j),IzTest(1,k));

                x0 = [x(1:6,1);IzTest(1,k)];
            
                ynn = zeros(6,length(timespan));
                ynn(:,1)=x(1:6,1);
            
                y0 = zeros(6,1);
            
                for jj=2:(size(timespan,2))
                    for l = 1:6
                        y0(l,1) = predict(nets{l,1},x0);
                    end
                    ynn(:,jj) = y0;
                    x0 = [y0;IzTest(1,k)];
                end
            
                err = x(1:6,:)-ynn;
                err = err.^2;
                err = sum(err,2)/(size(err,2));
                err = sqrt(err);
                
                errMat(i,j,k,:) = err;

                print_subplot(1,'q_{1} [rad]',err,x,ynn,timespan);
                print_subplot(2,'q_{2} [rad]',err,x,ynn,timespan);
                print_subplot(3,'q_{3} [rad]',err,x,ynn,timespan);
                print_subplot(4,'\omega_{1} [rad/s]',err,x,ynn,timespan);
                print_subplot(5,'\omega_{2} [rad/s]',err,x,ynn,timespan);
                print_subplot(6,'\omega_{3} [rad/s]',err,x,ynn,timespan);

            end
        end
    end
    errAvg = sum(sum(sum(errMat,1),2),3)/(size(wy0Test,2)*size(wz0Test,2)*size(IzTest,2));
    save('GyroPSSHyper2ProperInertiaVarTest.mat','errMat','errAvg','nets','wy0Test','wz0Test','IzTest');

end

function print_subplot(index,name,err,x,ynn,t)
    sHandle = subplot(2,3,index);
    cla
    axis equal;box on, hold on;
    sHandle.XGrid = 'on';
    sHandle.YGrid = 'on';
    sHandle.YLabel.HorizontalAlignment = 'right';

    str = sprintf("RMSE = %s",err(index));
    title(str);

    plot(t(1:end),x(index,:),'b')
    plot(t(1:end),ynn(index,:),':','Linewidth',2,'Color','r')
    legend('ODE solver','NN');
    xlabel('$\emph{t} [s]$','Interpreter','latex');
    ylabel(['$',name,'$'],'Interpreter','latex','Rotation',0);
end

function [input,output] = get_multiple_trajectories(model,wx0,wy0,wz0,Iz,max_t,smpFreq)
    trajLength = max_t*smpFreq;

    input = zeros(7,trajLength*(size(wy0,2)*size(wz0,2)-1));
    output = zeros(6,trajLength*(size(wy0,2)*size(wz0,2)-1));
    offset = 0;
    for i=1:size(wy0,2)
        for j=1:size(wz0,2)
            for k=1:size(Iz,2)
                if(wy0(i) == 0 && wz0(j) == 0)
                    a = 5;
                else
                    trajectory = get_single_trajectory(model,wx0,wy0(1,i),wz0(1,j),Iz(1,k));
    
                    input(1:6,1+offset:trajLength + offset) = trajectory(1:6,1:end-1);
                    input(7,1+offset:trajLength + offset) = ones(1,trajLength)*Iz(1,k);
                    output(:,1+offset:trajLength + offset) = trajectory(1:6,2:end);
    
                    offset = offset + trajLength;
                end
            end
        end
    end
end

function init_SimScape_Model(model,smpFreq,max_t)
    set_param(model,'FastRestart','off');
    set_param([model , '/Gimbal Joint'], 'RxVelocityTargetValue_conf', 'runtime')
    set_param([model , '/Gimbal Joint'], 'RyVelocityTargetValue_conf', 'runtime')
    set_param([model , '/Gimbal Joint'], 'RzVelocityTargetValue_conf', 'runtime')
    set_param([model , '/Cylindrical Solid'], 'MomentsOfInertia_conf', 'runtime')

    
    set_param(model,'FixedStep',num2str(1/smpFreq));
    set_param(model,'StopTime', num2str(max_t));
    set_param(model,'FastRestart','on');
end

function [trajectory] = get_single_trajectory(model,wx0,wy0,wz0,Iz)
        set_param('GyroscopeDraft/Gimbal Joint','RxVelocityTargetValue',num2str(wx0));
        set_param('GyroscopeDraft/Gimbal Joint','RyVelocityTargetValue',num2str(wy0));
        set_param('GyroscopeDraft/Gimbal Joint','RzVelocityTargetValue',num2str(wz0));
        set_param('GyroscopeDraft/Cylindrical Solid', 'MomentsOfInertia', ['[',num2str([1,1,Iz]),']']);

        
        simulation=sim(model);
        results=simulation.simout;
        trajectory = results(1).Data;
        trajectory = trajectory.';
        trajectory = [trajectory(4:6,:);trajectory(1:3,:)];
end