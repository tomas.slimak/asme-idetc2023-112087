# Description

Folder contains following scripts and data:

* Training scripts for gyroscope "branched" NN architecture
* Training scripts for gyroscope "parallel" NN architecture (with and without main axis rotational inertia variation)
* Training scripts for standard gyroscope NN architecture

