smpFreq = 1000;
max_t = 1;
model = 'Gyroscope';


wx0 = 0;
wy0 = 1;
wz0 = 5;

init_SimScape_Model(model,smpFreq,max_t);
%trajectory = get_single_trajectory(model,wx0,wy0,wz0);

search_net_PSS(max_t,smpFreq,model)

%load('SearchGyroPSS/GyroSearch_1654494.mat');

%test_net(model,nets,wy0,wz0,max_t,smpFreq)

function search_net_PSS(max_t,smpFreq,model)
    wx0 = 0;

    wy0 = 1:0.1:1.5; 
    wz0 = 5:0.5:7;

    nets = cell(6,2);

    [input,output] = get_multiple_trajectories(model,wx0,wy0,wz0,max_t,smpFreq);

    partition = floor(0.8*size(input,2));

    inputT = input(:,1:partition-1);
    outputT = output(:,1:partition-1);

    inputV = input(:,partition:end);
    outputV = output(:,partition:end);

    maxEpochs = 500;
    options = trainingOptions('adam', ...
    'ExecutionEnvironment','gpu', ...
    'MaxEpochs',maxEpochs, ...
    'OutputNetwork','best-validation-loss',...
    'ValidationFrequency',10, ...
    'Shuffle','every-epoch', ...
    'LearnRateSchedule','piecewise', ...
    'GradientDecayFactor',0.90,...
    'SquaredGradientDecayFactor',0.99,...
    'InitialLearnRate',0.0274,...
    'LearnRateDropFactor',0.9606, ...
    'LearnRateDropPeriod',22, ...
    'L2Regularization',0.2,...
    'MiniBatchSize',128,...
    'ValidationData',{inputV,outputV});
    for ii=1:2000
        bestTrain = 0;
        FCL1 =  12 + round(24*(rand(1,1)));
        FCL2 =  6 + round(12*(rand(1,1)));

        layers = [ ...
        sequenceInputLayer(6)
        fullyConnectedLayer(FCL1)
        functionLayer(@(x) sin(x))
        fullyConnectedLayer(FCL2)
        fullyConnectedLayer(1)
        regressionLayer
        ];
        
        layers2 = [ ...
        sequenceInputLayer(6)
        fullyConnectedLayer(FCL1)
        reluLayer
        fullyConnectedLayer(FCL2)
        fullyConnectedLayer(1)
        regressionLayer
        ];

        layersC = cell(6,1);
        layersC{1,1} = layers;
        layersC{2,1} = layers;
        layersC{3,1} = layers;
        layersC{4,1} = layers;
        layersC{5,1} = layers;
        layersC{6,1} = layers2;
        
        for i=1:6
            options.ValidationData = {inputV,outputV(i,:)};
            [net,info] = trainNetwork(inputT,outputT(i,:),layersC{i,1},options); 
            nets{i,1} = net;
            nets{i,2} = info;

            iteration = info.OutputNetworkIteration;
            bestTrainLocal = (round(info.ValidationLoss(iteration)*1000000000));
            bestTrain = bestTrain + bestTrainLocal;
        end
        bestTrain = num2str(bestTrain);
        save(['SearchGyroPSS/GyroSearch_',bestTrain,'.mat'],'nets','options','max_t','smpFreq','layersC','wx0','wy0','wz0');

    end
end

function test_net(model,nets,wy0,wz0,max_t,smpFreq)
    timespan = 0:1/smpFreq:max_t;

    wx0 = 0;

    wy0Test = randi([wy0(1)*100,wy0(end)*100],1,1)/100
    wz0Test = randi([wz0(1)*100,wz0(end)*100],1,1)/100

    x = get_single_trajectory(model,wx0,wy0Test,wz0Test);

    x0 = x(1:6,1);

    ynn = zeros(6,length(timespan));
    ynn(:,1)=x(1:6,1);

    y0 = zeros(6,1);

    for jj=2:(size(timespan,2))
        for k = 1:6
            y0(k,1) = predict(nets{k,1},x0);
        end
        ynn(:,jj) = y0;
        x0 = y0;
    end

    err = x(1:6,:)-ynn;
    err = err.^2;
    err = sum(err,2)/(size(err,2));
    err = sqrt(err);

    figure(1);
    cla
    print_subplot(1,'q_{1} [rad]',err,x,ynn,timespan);
    print_subplot(2,'q_{2} [rad]',err,x,ynn,timespan);
    print_subplot(3,'q_{3} [rad]',err,x,ynn,timespan);
    print_subplot(4,'\omega_{1} [rad/s]',err,x,ynn,timespan);
    print_subplot(5,'\omega_{2} [rad/s]',err,x,ynn,timespan);
    print_subplot(6,'\omega_{3} [rad/s]',err,x,ynn,timespan);

end

function print_subplot(index,name,err,x,ynn,t)
    sHandle = subplot(2,3,index);
    cla
    axis equal;box on, hold on;
    sHandle.XGrid = 'on';
    sHandle.YGrid = 'on';
    sHandle.YLabel.HorizontalAlignment = 'right';

    str = sprintf("RMSE = %s",err(index));
    title(str);

    plot(t(1:end),x(index,:),'b')
    plot(t(1:end),ynn(index,:),':','Linewidth',2,'Color','r')
    legend('ODE solver','NN');
    xlabel('$\emph{t} [s]$','Interpreter','latex');
    ylabel(['$',name,'$'],'Interpreter','latex','Rotation',0);
end

function [input,output] = get_multiple_trajectories(model,wx0,wy0,wz0,max_t,smpFreq)
    trajLength = max_t*smpFreq;

    input = zeros(6,trajLength*(size(wy0,2)*size(wz0,2)-1));
    output = zeros(6,trajLength*(size(wy0,2)*size(wz0,2)-1));
    offset = 0;
    for i=1:size(wy0,2)
        for j=1:size(wz0,2)
            if(wy0(i) == 0 && wz0(j) == 0)
                a = 5;
            else
                trajectory = get_single_trajectory(model,wx0,wy0(1,i),wz0(1,j));

                input(:,1+offset:trajLength + offset) = trajectory(1:6,1:end-1);
                output(:,1+offset:trajLength + offset) = trajectory(1:6,2:end);

                offset = offset + trajLength;
            end
        end
    end
end

function init_SimScape_Model(model,smpFreq,max_t)
    set_param(model,'FastRestart','off');
    set_param([model , '/Gimbal Joint'], 'RxVelocityTargetValue_conf', 'runtime')
    set_param([model , '/Gimbal Joint'], 'RyVelocityTargetValue_conf', 'runtime')
    set_param([model , '/Gimbal Joint'], 'RzVelocityTargetValue_conf', 'runtime')
    
    set_param(model,'FixedStep',num2str(1/smpFreq));
    set_param(model,'StopTime', num2str(max_t));
    set_param(model,'FastRestart','on');
end

function [trajectory] = get_single_trajectory(model,wx0,wy0,wz0)
        set_param('GyroscopeDraft/Gimbal Joint','RxVelocityTargetValue',num2str(wx0));
        set_param('GyroscopeDraft/Gimbal Joint','RyVelocityTargetValue',num2str(wy0));
        set_param('GyroscopeDraft/Gimbal Joint','RzVelocityTargetValue',num2str(wz0));
        
        simulation=sim(model);
        results=simulation.simout;
        trajectory = results(1).Data;
        trajectory = trajectory.';
        trajectory = [trajectory(4:6,:);trajectory(1:3,:)];
end