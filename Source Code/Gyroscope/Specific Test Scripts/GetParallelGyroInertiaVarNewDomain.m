smpFreq = 100;
max_t = 1;
model = 'Gyroscope';

wx0 = 0;
wy0 = 1;
wz0 = 5;

init_SimScape_Model(model,smpFreq,max_t);

load('GyroParallelInertiaVarNewDomain.mat');

%test_net(model,nets,wy0,wz0,Iz,max_t,smpFreq)

load('testData.mat');
gather_extra_data_prescribed(model,nets,smpFreq,max_t,testParameters)

function [errRMSE,errRAE] = test_net(model,nets,wy0,Iz,max_t,smpFreq)
    timespan = 0:1/smpFreq:max_t;

    wx0 = 0;

    wy0Test = wy0;
    wz0Test = 6;
    IzTest = Iz;

    x = get_single_trajectory(model,wx0,wy0Test,wz0Test,IzTest);

    x0 = [x(1:6,1);IzTest];

    ynn = zeros(6,length(timespan));
    ynn(:,1)=x(1:6,1);

    y0 = zeros(6,1);

    for jj=2:(size(timespan,2))
        for k = 1:6
            y0(k,1) = predict(nets{k,1},x0);
        end
        %y0(3,1) = x(3,jj);%
        ynn(:,jj) = y0;
        x0 = [y0;IzTest];
    end

    err = x(1:6,:)-ynn;
    err = err.^2;
    err = sum(err,2)/(size(err,2));
    err = sqrt(err);
    errRMSE = err;
    errRAE = sum(abs(x(1:6,:)-ynn),2)./sum(abs(x(1:6,:)),2);

    figure(1);
    cla
    print_subplot(1,'q_{1} [rad]',err,x,ynn,timespan);
    print_subplot(2,'q_{2} [rad]',err,x,ynn,timespan);
    print_subplot(3,'q_{3} [rad]',err,x,ynn,timespan);
    print_subplot(4,'\omega_{1} [rad/s]',err,x,ynn,timespan);
    print_subplot(5,'\omega_{2} [rad/s]',err,x,ynn,timespan);
    print_subplot(6,'\omega_{3} [rad/s]',err,x,ynn,timespan);

end

function gather_extra_data_prescribed(model,nets,smpFreq,max_t,testParameters)

    numTests = size(testParameters,3);

    RMSEExtraData = zeros(size(testParameters,1),size(testParameters,2),numTests,6);
    RAEExtraData = zeros(size(testParameters,1),size(testParameters,2),numTests,6);
    for i=1:size(testParameters,1)
        for j=1:size(testParameters,2)

            wy0Test = testParameters(i,j,:,1);
            IzTest = testParameters(i,j,:,2);
            for k=1:numTests
                [errRMSE,errRAE] = test_net(model,nets,wy0Test(1,k),IzTest(1,k),max_t,smpFreq);
                RMSEExtraData(i,j,k,:) = errRMSE;
                RAEExtraData(i,j,k,:) = errRAE;
            end 
        end
    end
    RMSEExtraDataAvg = squeeze(sum(RMSEExtraData,3)/numTests);
    RAEExtraDataAvg = squeeze(sum(RAEExtraData,3)/numTests);
    save(['ExtraDataNewDomain',num2str(numTests),'.mat'],'RMSEExtraData','RAEExtraDataAvg','RMSEExtraDataAvg','testParameters');
end

function print_subplot(index,name,err,x,ynn,t)
    sHandle = subplot(2,3,index);
    cla
    axis equal;box on, hold on;
    sHandle.XGrid = 'on';
    sHandle.YGrid = 'on';
    sHandle.YLabel.HorizontalAlignment = 'right';

    str = sprintf("RMSE = %s",err(index));
    title(str);

    plot(t(1:end),x(index,:),'b')
    plot(t(1:end),ynn(index,:),':','Linewidth',2,'Color','r')
    legend('ODE solver','NN');
    xlabel('$\emph{t} [s]$','Interpreter','latex');
    ylabel(['$',name,'$'],'Interpreter','latex','Rotation',0);
end

function init_SimScape_Model(model,smpFreq,max_t)
    set_param(model,'FastRestart','off');
    set_param([model , '/Gimbal Joint'], 'RxVelocityTargetValue_conf', 'runtime')
    set_param([model , '/Gimbal Joint'], 'RyVelocityTargetValue_conf', 'runtime')
    set_param([model , '/Gimbal Joint'], 'RzVelocityTargetValue_conf', 'runtime')
    set_param([model , '/Cylindrical Solid'], 'MomentsOfInertia_conf', 'runtime')

    
    set_param(model,'FixedStep',num2str(1/smpFreq));
    set_param(model,'StopTime', num2str(max_t));
    set_param(model,'FastRestart','on');
end

function [trajectory] = get_single_trajectory(model,wx0,wy0,wz0,Iz)
        set_param('Gyroscope/Gimbal Joint','RxVelocityTargetValue',num2str(wx0));
        set_param('Gyroscope/Gimbal Joint','RyVelocityTargetValue',num2str(wy0));
        set_param('Gyroscope/Gimbal Joint','RzVelocityTargetValue',num2str(wz0));
        set_param('Gyroscope/Cylindrical Solid', 'MomentsOfInertia', ['[',num2str([1,1,Iz]),']']);

        
        simulation=sim(model);
        results=simulation.simout;
        trajectory = results(1).Data;
        trajectory = trajectory.';
        trajectory = [trajectory(4:6,:);trajectory(1:3,:)];
        %trajectory = [trajectory(4:6,:);trajectory(7:9,:)];
end