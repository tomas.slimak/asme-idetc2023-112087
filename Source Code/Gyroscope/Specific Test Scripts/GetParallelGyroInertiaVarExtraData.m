smpFreq = 100;
max_t = 1;
model = 'Gyroscope';

wx0 = 0;
wy0 = 1;
wz0 = 5;

init_SimScape_Model(model,smpFreq,max_t);

load('GyroParallelInertiaVar.mat');

%test_net(model,nets,wy0,wz0,Iz,max_t,smpFreq)

gather_extra_data(model,nets,max_t,smpFreq)

function [errRMSE,errRAE] = test_net(model,nets,wy0,Iz,max_t,smpFreq)
    timespan = 0:1/smpFreq:max_t;

    wx0 = 0;

    wy0Test = wy0;
    wz0Test = 6;
    IzTest = Iz;

    x = get_single_trajectory(model,wx0,wy0Test,wz0Test,IzTest);

    x0 = [x(1:6,1);IzTest];

    ynn = zeros(6,length(timespan));
    ynn(:,1)=x(1:6,1);

    y0 = zeros(6,1);

    for jj=2:(size(timespan,2))
        for k = 1:6
            y0(k,1) = predict(nets{k,1},x0);
        end
        %y0(3,1) = x(3,jj);%
        ynn(:,jj) = y0;
        x0 = [y0;IzTest];
    end

    err = x(1:6,:)-ynn;
    err = err.^2;
    err = sum(err,2)/(size(err,2));
    err = sqrt(err);
    errRMSE = err;
    errRAE = sum(abs(x(1:6,:)-ynn),2)./sum(abs(x(1:6,:)),2);

    figure(1);
    cla
    print_subplot(1,'q_{1} [rad]',err,x,ynn,timespan);
    print_subplot(2,'q_{2} [rad]',err,x,ynn,timespan);
    print_subplot(3,'q_{3} [rad]',err,x,ynn,timespan);
    print_subplot(4,'\omega_{1} [rad/s]',err,x,ynn,timespan);
    print_subplot(5,'\omega_{2} [rad/s]',err,x,ynn,timespan);
    print_subplot(6,'\omega_{3} [rad/s]',err,x,ynn,timespan);

end

function gather_extra_data(model,nets,max_t,smpFreq)
    wy0 = 0:0.5:5; 
    Iz = 0:1:7;
    
    numTests = 10;

    RMSEExtraData = zeros(numel(wy0)-1,numel(Iz)-1,numTests,6);
    RAEExtraData = zeros(numel(wy0)-1,numel(Iz)-1,numTests,6);
    testParameters = zeros(numel(wy0)-1,numel(Iz)-1,numTests,2);

    for i=1:numel(wy0)-1
        for j=1:numel(Iz)-1

            wy0Min = wy0(1,i);
            wy0Max = wy0(1,i+1);
            IzMin = Iz(1,j);
            IzMax = Iz(1,j+1);

            wy0Test = randi([int32(wy0Min*100),int32(wy0Max*100)],1,numTests)/100;
            IzTest = randi([int32(IzMin*100),int32(IzMax*100)],1,numTests)/100;
            testParameters(i,j,:,1) = wy0Test;
            testParameters(i,j,:,2) = IzTest;
            for k=1:numTests
                tic
                [errRMSE,errRAE] = test_net(model,nets,wy0Test(1,k),IzTest(1,k),max_t,smpFreq);
                toc
                RMSEExtraData(i,j,k,:) = errRMSE;
                RAEExtraData(i,j,k,:) = errRAE;
            end
        end
    end
    RMSEExtraDataAvg = squeeze(sum(RMSEExtraData,3)/numTests);
    RAEExtraDataAvg = squeeze(sum(RAEExtraData,3)/numTests);
    save('ExtraDataAvg.mat','RMSEExtraData','RAEExtraDataAvg','RMSEExtraDataAvg','testParameters','wy0','Iz');
    save('testData.mat','testParameters');
end

function print_subplot(index,name,err,x,ynn,t)
    sHandle = subplot(2,3,index);
    cla
    axis equal;box on, hold on;
    sHandle.XGrid = 'on';
    sHandle.YGrid = 'on';
    sHandle.YLabel.HorizontalAlignment = 'right';

    str = sprintf("RMSE = %s",err(index));
    title(str);

    plot(t(1:end),x(index,:),'b')
    plot(t(1:end),ynn(index,:),':','Linewidth',2,'Color','r')
    legend('ODE solver','NN');
    xlabel('$\emph{t} [s]$','Interpreter','latex');
    ylabel(['$',name,'$'],'Interpreter','latex','Rotation',0);
end

function init_SimScape_Model(model,smpFreq,max_t)
    set_param(model,'FastRestart','off');
    set_param([model , '/Gimbal Joint'], 'RxVelocityTargetValue_conf', 'runtime')
    set_param([model , '/Gimbal Joint'], 'RyVelocityTargetValue_conf', 'runtime')
    set_param([model , '/Gimbal Joint'], 'RzVelocityTargetValue_conf', 'runtime')
    set_param([model , '/Cylindrical Solid'], 'MomentsOfInertia_conf', 'runtime')

    
    set_param(model,'FixedStep',num2str(1/smpFreq));
    set_param(model,'StopTime', num2str(max_t));
    set_param(model,'FastRestart','on');
end

function [trajectory] = get_single_trajectory(model,wx0,wy0,wz0,Iz)
        set_param('Gyroscope/Gimbal Joint','RxVelocityTargetValue',num2str(wx0));
        set_param('Gyroscope/Gimbal Joint','RyVelocityTargetValue',num2str(wy0));
        set_param('Gyroscope/Gimbal Joint','RzVelocityTargetValue',num2str(wz0));
        set_param('Gyroscope/Cylindrical Solid', 'MomentsOfInertia', ['[',num2str([1,1,Iz]),']']);

        
        simulation=sim(model);
        results=simulation.simout;
        trajectory = results(1).Data;
        trajectory = trajectory.';
        trajectory = [trajectory(4:6,:);trajectory(1:3,:)];
        %trajectory = [trajectory(4:6,:);trajectory(7:9,:)];
end