g = 9.81;
m_2 = 2;
m_1 = 2;
l_1 = 2;
l_2 = 3;

max_t = 5;
snapsPerS = 100;

double_penudlum = @(t,x)double_pendulum_system(x,l_1,l_2,m_1,m_2,g);

noisePercent = 0;

train_net_stepbehind_EN_Noise(double_penudlum,max_t,snapsPerS,noisePercent)

load('DPdnnSbeh8EN_Noise_0Percent.mat');

test_net_stepbehind_EN_Noise(double_penudlum,net,max_t,snapsPerS,th1,th2,SBeh)

function train_net_stepbehind_EN_Noise(double_penudlum,max_t,snapsPerS,noisePercent)

    th1 = 0:1:4;
    th2 = -5:1:5;

    SBeh = 8;

    [input,output] = generate_data_stepsbehind_state(th1,th2,max_t,snapsPerS,double_penudlum,SBeh,noisePercent);

    partition = floor(0.8*size(input,2));

    inputT = input(:,1:partition-1);
    outputT = output(:,1:partition-1);

    inputV = input(:,partition:end);
    outputV = output(:,partition:end);

    maxEpochs = 3500;
    options = trainingOptions('adam', ...
    'ExecutionEnvironment','gpu', ...
    'MaxEpochs',maxEpochs, ...
    'OutputNetwork','best-validation-loss',...
    'ValidationFrequency',10, ...
    'Shuffle','every-epoch', ...
    'LearnRateSchedule','piecewise', ...
    'GradientDecayFactor',0.90,...
    'SquaredGradientDecayFactor',0.99,...
    'InitialLearnRate',0.0186,... % 0.0069
    'LearnRateDropFactor',0.9081, ... %0.9539
    'LearnRateDropPeriod',24, ... %42
    'L2Regularization',0.2,...
    'MiniBatchSize',128,...
    'ValidationData',{inputV,outputV});

    alpha = 0;

    layers = [ ...
        sequenceInputLayer(SBeh*4)
        fullyConnectedLayer(11)
        reluLayer
        fullyConnectedLayer(286)
        fullyConnectedLayer(4)
        PIRegressionLayerMod(alpha)
    ];

    tic
    trainNetwork(inputT,outputT,layers,options);
    tNN = toc;
    fprintf('NN Training Time: %f s \n',tNN);

end

function test_net_stepbehind_EN_Noise(double_penudlum,net,max_t,snapsPerS,th1,th2,SBeh)
    timespan = 0:1/snapsPerS:max_t;

    err = 0;

    th1test = randi([th1(1)*10,th1(end)*10],1,1);
    th2test = randi([th2(1)*10,th2(end)*10],1,1);

    %% SBeh vs Std Showcase:
    th1test = 40;
    th2test = 49;

    x0 = [pi/3*((th1test)/100),pi/3*((th2test)/100),0,0];

    [t,x] = ode45(double_penudlum,timespan ,x0(end,1:4)');

    x = x.';
    t = t.';
    x0 = zeros(4*SBeh,1);
    
    offsetSBeh = 0;
    for i=1:SBeh
        x0(1 + offsetSBeh:4 + offsetSBeh,1) = x(:,i);
        offsetSBeh = offsetSBeh + 4;
    end

    ynn = zeros(4,length(timespan));
    ynn(:,1:SBeh)=x(:,1:SBeh);
    
    timing = zeros(1,max_t*snapsPerS+1);
    for jj=SBeh + 1:(size(timespan,2))
        tic
        y0 = predict(net,x0);
        timing(1,jj) = toc;
        ynn(:,jj) = y0(1:4);
        x0(1:(SBeh-1)*4,1) = x0(5:end,1);
        x0((SBeh-1)*4 + 1:end,1) = y0;
    end
    fprintf('NN Prediction Time: %f s \n',mean(timing(1,2:end)));
end

function [input,output] = generate_data_stepsbehind_state(th1,th2,max_t,snapsPerS,double_penudlum,SBeh,noisePercent)
    numsamples = length(th1)*length(th2)-1;
    smpLength = max_t*snapsPerS + 1 - SBeh;

    input = zeros(4*SBeh,smpLength*numsamples);
    output = zeros(4,smpLength*numsamples);

    timespan = 0:1/snapsPerS:max_t;
    
    timing = zeros(1,1);
    counter = 1;
    offsetSmp = 0;
    for i=1:length(th1)
        for j=1:length(th2)
           
            x = [(pi/3)*(th1(i)/10),(pi/3)*(th2(j)/10),0,0];
            if (x == [0,0,0,0])
                a = 5;
            else
                tic
                [t,x] = ode45(double_penudlum,timespan,x(end,:)');
                timing(1,counter) = toc;
                counter = counter + 1;
                x = x.';

                maxVal = max(abs(x.'));
                maxVal = maxVal.';

                x = x + (maxVal*(noisePercent/100)).*randn(size(x));

                offsetSBeh = 0;
                for k=1:SBeh
                    input(1 + offsetSBeh:4 + offsetSBeh,1 + offsetSmp:smpLength + offsetSmp) = x(:,1 - 1 + k:smpLength - 1 + k);
                    offsetSBeh = offsetSBeh + 4;
                end
                output(:,1 + offsetSmp:smpLength + offsetSmp) = x(:,1 + SBeh:smpLength + SBeh);
                offsetSmp = offsetSmp + smpLength;
            end
        end
    end
    fprintf('Mean Time per Trajectory: %fs \n',mean(timing(1,2:end)));
end

function [input,output] = generate_data(th1,th2,max_t,snapsPerS,double_penudlum)

    timespan = 0:(1/snapsPerS):max_t;

    smpLength = size(timespan,2) - 1;
    numsamples = length(th1)*length(th2)-1;

    input = zeros(smpLength*numsamples,4);
    output = zeros(smpLength*numsamples,4);
    smpCounter = 1;

    for i=1:length(th1)
        for j=1:length(th2)
           
            x = [(pi/3)*(th1(i)/10),(pi/3)*(th2(j)/10),0,0];
            if (x == [0,0,0,0])
                
            else
                [t,x] = ode45(double_penudlum,timespan ,x(end,:)');

                offset = (smpCounter-1)*smpLength;
                input(offset+1:offset + smpLength,:) = x(1:end-1,:);
                output(offset+1:offset + smpLength,:) = x(2:end,:);
                smpCounter = smpCounter + 1;
            end
        end
    end
    input = input.';
    output = output.';
end

function dq = double_pendulum_system(x,l_1,l_2,m_1,m_2,g)
    theta1 = x(1);
    theta2 = x(2);
    omega1 = x(3);
    omega2 = x(4);

    dtheta1 = omega1;
    dtheta2 = omega2;
    domega1 = (g*m_2*sin(theta1 + 2*theta2) - g*m_2*sin(theta1) - 2*g*m_1*sin(theta1) + l_1*m_2*omega1^2*sin(2*theta2) + 2*l_2*m_2*omega1^2*sin(theta2) + 2*l_2*m_2*omega2^2*sin(theta2) + 4*l_2*m_2*omega1*omega2*sin(theta2))/(2*l_1*m_1 + l_1*m_2 - l_1*m_2*cos(2*theta2)); 
    domega2 = -(l_1^2*m_1*omega1^2*sin(theta2) + l_1^2*m_2*omega1^2*sin(theta2) + l_2^2*m_2*omega1^2*sin(theta2) + l_2^2*m_2*omega2^2*sin(theta2) - g*l_2*m_1*sin(theta1) - g*l_2*m_2*sin(theta1) + 2*l_2^2*m_2*omega1*omega2*sin(theta2) + g*l_1*m_1*cos(theta1)*sin(theta2) + g*l_1*m_2*cos(theta1)*sin(theta2) + l_1*l_2*m_2*omega1^2*sin(2*theta2) + (l_1*l_2*m_2*omega2^2*sin(2*theta2))/2 + g*l_2*m_2*cos(theta2)^2*sin(theta1) + g*l_2*m_2*cos(theta1)*cos(theta2)*sin(theta2) + l_1*l_2*m_2*omega1*omega2*sin(2*theta2))/(l_1*l_2*(m_1 + m_2 - m_2*cos(theta2)^2)); 

    dq = [dtheta1;dtheta2;domega1;domega2];
end