classdef PIRegressionLayerMod < nnet.layer.RegressionLayer    
    properties
        alpha
    end
    methods
        function layer = PIRegressionLayerMod(alpha)
            layer.Description = 'Energy-based regression Layer with + inst. *';
            layer.alpha = alpha;
        end
        
        function loss = forwardLoss(layer, Y, T)
            R = size(Y,3);
            a = 78.48;
            b = 58.86;
            c = 17;
            d = 9;
            e = 12;

            %alpha = 0.01;



        
            %Err = T-Y;

            Ey = a*(1-cos(Y(1,:,:))) +  b*(1-cos(Y(1,:,:) + Y(2,:,:)))...
                + c*Y(3,:,:).^2 + d*Y(4,:,:).^2 ...
                + e*((Y(3,:,:).^2 + Y(3,:,:).*Y(4,:,:)).*cos(Y(2,:,:)))...
                + 2*d*(Y(3,:,:).*Y(4,:,:));

            Et = a*(1-cos(T(1,:,:))) +  b*(1-cos(T(1,:,:) + T(2,:,:)))...
                + c*T(3,:,:).^2 + d*T(4,:,:).^2 ...
                + e*((T(3,:,:).^2 + T(3,:,:).*T(4,:,:)).*cos(T(2,:,:)))...
                + 2*d*(T(3,:,:).*T(4,:,:));

            EErr = Ey-Et;
            SEErr = EErr.^2;
            MSEErr = sum(SEErr,3);



            %PEErr = abs(EErr)./(abs(Et));
            %MSEW = (sum(Err.^2,1)).*(1+PEErr);

            %SErr = Err.^2;
            %MSErr = sum(SErr,3)/size(SErr,3);
            %MSErr = sum(MSErr,1);
            
            loss = sum((sum(((T-Y).^2),3)),1) + MSEErr*layer.alpha;
            %loss = sum((sum(((T-Y).^2),3)/sum(size(T,3))),1);

            %loss = MSErr;% + alpha*MSEErr;
        end
    end
end