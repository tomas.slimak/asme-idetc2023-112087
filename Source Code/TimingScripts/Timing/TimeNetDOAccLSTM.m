smpFreq = 100;
max_t = 20;
model = 'DuffingOscilatorAcc';

x0 = 0;
v0 = 0;

m = 1;
l = 2;

alpha = -1;
beta = 1;
delta = 0.3;

init_SimScape_Model(model,smpFreq,max_t);

train_net_grid(model,max_t,smpFreq,m,l,alpha,beta,delta);

load('DOForcedLSTMAcc20s.mat')

test_net(model,net,x0,v0,m,l,alpha,beta,gamma,delta,omega,max_t,smpFreq)

function train_net_grid(model,max_t,smpFreq,m,l,alpha,beta,delta)

    x0 = 0;
    v0 = 0;

    gamma = 0.1:0.1:0.5;
    omega = 0.4:0.1:1.5;

    [input,output] = get_multiple_trajectories(model,x0,v0,m,l,alpha,beta,gamma,delta,omega,max_t,smpFreq);

    partition = floor(0.8*size(input,2));

    inputT = input(:,1:partition-1);
    outputT = output(:,1:partition-1);

    inputV = input(:,partition:end);
    outputV = output(:,partition:end);

    maxEpochs = 200;
    options = trainingOptions('adam', ...
    'ExecutionEnvironment','gpu', ...
    'MaxEpochs',maxEpochs, ...
    'OutputNetwork','best-validation-loss',...
    'ValidationFrequency',10, ...
    'Shuffle','every-epoch', ...
    'LearnRateSchedule','piecewise', ...
    'GradientDecayFactor',0.90,...
    'SquaredGradientDecayFactor',0.99,...
    'InitialLearnRate',0.0074,... 
    'LearnRateDropFactor',0.9606,... 
    'LearnRateDropPeriod',37,...
    'L2Regularization',0.2,...
    'MiniBatchSize',128,...
    'ValidationData',{inputV,outputV});
    

    layers = [ ...
    sequenceInputLayer(3)
    lstmLayer(36)
    fullyConnectedLayer(83)
    leakyReluLayer(0.1)
    fullyConnectedLayer(46)
    fullyConnectedLayer(1)
    regressionLayer
    ];

    tic
    trainNetwork(inputT,outputT,layers,options);
    tNN = toc;
    fprintf('NN Training Time: %f s \n',tNN);
end

function test_net(model,net,x0,v0,m,l,alpha,beta,gamma,delta,omega,max_t,smpFreq)
    timespan = 0:1/smpFreq:max_t;

    gammaTest = randi([int32(gamma(1)*100),int32(gamma(end)*100)],1,1)/100;
    omegaTest = randi([int32(omega(1)*100),int32(omega(end)*100)],1,1)/100;

    gammaTest = 1.2;
    omegaTest = 8;

    x = get_single_trajectory(model,x0,v0,m,l,alpha,beta,gammaTest,delta,omegaTest);

    x0 = [x(1:2,1);x(3,1)];

    ynn = zeros(2,length(timespan));
    ynn(:,1)=x0(1:2);

    net = resetState(net);

    timing = zeros(1,max_t*smpFreq+1);
    for jj=2:(size(timespan,2)) 
        tic
        [net,y0] = predictAndUpdateState(net,x0);
        timing(1,jj) = toc;
        xadv_v = x0(2,1) + y0*(1/smpFreq);
        xadv_x = x0(1,1) + x0(2,1)*(1/smpFreq);
        x0 = [xadv_x;xadv_v;x(4,jj)];
        ynn(:,jj) = x0(1:2);
    end
    fprintf('NN Prediction Time: %f s \n',mean(timing(1,2:end)));
end

function [input,output] = get_multiple_trajectories(model,x0,v0,m,l,alpha,beta,gamma,delta,omega,max_t,smpFreq)
    trajLength = max_t*smpFreq+1;

    input = zeros(3,trajLength*(size(gamma,2)*size(omega,2)-1));
    output = zeros(1,trajLength*(size(gamma,2)*size(omega,2)-1));
    offset = 0;
    counter = 1;
    timing = zeros(1,1);

    for i=1:size(gamma,2)
        for j=1:size(omega,2)
            if(gamma(i) == 0 && omega(j) == 0)
                a = 5;
            else
                tic
                trajectory = get_single_trajectory(model,x0,v0,m,l,alpha,beta,gamma(1,i),delta,omega(1,j));
                timing(1,counter) = toc;
                counter = counter + 1;
                input(1:2,1+offset:trajLength + offset) = trajectory(1:2,1:end);
                input(3,1+offset:trajLength + offset) = trajectory(4,1:end);

                output(:,1+offset:trajLength + offset) = trajectory(3,1:end);
                offset = offset + trajLength;
            end
        end
    end
    fprintf('Mean Time per Trajectory: %fs \n',mean(timing(1,2:end)));
end

function init_SimScape_Model(model,smpFreq,max_t)
    set_param(model,'FastRestart','off');
    set_param([model , '/Prismatic Joint'], 'PositionTargetValue_conf', 'runtime')
    set_param([model , '/Prismatic Joint'], 'VelocityTargetValue_conf', 'runtime')
    
    set_param([model , '/Brick Solid'], 'BrickDimensions_conf', 'runtime')
    set_param([model , '/Brick Solid'], 'Mass_conf', 'runtime')

    set_param([model , '/Gain'], 'Gain', 'runtime')
    set_param([model , '/Gain1'], 'Gain', 'runtime')
    set_param([model , '/Gain2'], 'Gain', 'runtime')
    set_param([model , '/Gain3'], 'Gain', 'runtime')
    set_param([model , '/Gain4'], 'Gain', 'runtime')


    set_param(model,'FixedStep',num2str(1/smpFreq));
    set_param(model,'StopTime', num2str(max_t));
    set_param(model,'FastRestart','on');
end

function trajectory = get_single_trajectory(model,x0,v0,m,l,alpha,beta,gamma,delta,omega)
        set_param('DuffingOscilatorAcc/Prismatic Joint','PositionTargetValue',num2str(x0));
        set_param('DuffingOscilatorAcc/Prismatic Joint','VelocityTargetValue',num2str(v0));
        
        %set_param('DuffingOscilator/Brick Solid','BrickDimensions',['[',num2str([l,0.1,0.1]),']']);
        set_param('DuffingOscilatorAcc/Brick Solid','Mass',num2str(m));

        set_param('DuffingOscilatorAcc/Gain','Gain',num2str(-delta));
        set_param('DuffingOscilatorAcc/Gain1','Gain',num2str(-alpha));
        set_param('DuffingOscilatorAcc/Gain2','Gain',num2str(-beta));
        set_param('DuffingOscilatorAcc/Gain3','Gain',num2str(omega));
        set_param('DuffingOscilatorAcc/Gain4','Gain',num2str(-gamma));

        simulation=sim(model);
        results=simulation.simout;
        trajectory = results(1).Data;
        trajectory = trajectory.';
end