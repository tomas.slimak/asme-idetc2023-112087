%% FIRST BELOW THEN ABOVE
%% FIRST BELOW THEN ABOVE
%% FIRST BELOW THEN ABOVE

figure(1);

subHnd = cell(2,1);
subData = cell(2,3);

for i=1:2
    subHnd{i} =  subplot(2,1,i);
    subData{i,1} = subHnd{i}.Children(1).YData;
    subData{i,2} = subHnd{i}.Children(2).YData;
    %subData{i,3} = subHnd{i}.Title.String;
end
s1 = subplot(2,1,1);
s2 = subplot(2,1,2);

xData = s1.Children.XData;
y1DataNN = s1.Children(1).YData;
y2DataNN = s2.Children(1).YData;

y1DataODE = s1.Children(2).YData;
y2DataODE = s2.Children(2).YData;

MAE1 = sum((abs(y1DataNN-y1DataODE)),2)/sum(abs(y1DataODE),2)*100;
MAE2 = sum((abs(y2DataNN-y2DataODE)),2)/sum(abs(y2DataODE),2)*100;
MAE1 = sprintf('%.2g', MAE1);
MAE2 = sprintf('%.2g', MAE2);


figure(2);

subHnd = cell(2,1);
subData = cell(2,3);

for i=1:2
    subHnd{i} =  subplot(2,1,i);
    subData{i,1} = subHnd{i}.Children(1).YData;
    subData{i,2} = subHnd{i}.Children(2).YData;
    %subData{i,3} = subHnd{i}.Title.String;
end
s1 = subplot(2,1,1);
s2 = subplot(2,1,2);

y3DataNN = s1.Children(1).YData;
y4DataNN = s2.Children(1).YData;

y3DataODE = s1.Children(2).YData;
y4DataODE = s2.Children(2).YData;

MAE3 = sum((abs(y3DataNN-y3DataODE)),2)/sum(abs(y3DataODE),2)*100;
MAE4 = sum((abs(y4DataNN-y4DataODE)),2)/sum(abs(y4DataODE),2)*100;
MAE3 = sprintf('%.2g', MAE3);
MAE4 = sprintf('%.2g', MAE4);


f4 = figure(4);
%f4.WindowState = 'maximized';
f4.Position = [100 100 600 600];

t = tiledlayout(2,1);
t.TileSpacing = 'tight';
t.Padding = 'tight';

pause(1);
a = nexttile;
hold on;
plot(xData,y1DataNN,'Linewidth',2,'Color','#A2AD00'); %light green
plot(xData,y1DataODE,':','Linewidth',2,'Color','#A2AD00');

plot(xData,y3DataNN,'Linewidth',2,'Color','#0065BD'); % dark blue
plot(xData,y3DataODE,':','Linewidth',2,'Color','#0065BD');

%ylim([-1.5, 4.5])

%l1 = legend(stra,'Simscape','Location','northeast','FontSize',18,'Interpreter','latex');
%xlabel('$t$ [s]','Interpreter','latex','FontSize',18);
ylabel('Positions $x$ (m)','Interpreter','latex','FontSize',18);
a.TickLabelInterpreter = 'latex';
a.XGrid = 'on';
a.YGrid = 'on';
a.Box = 'on';
a.GridColor = [0 0 0];
a.GridAlpha = 0.15;
set(a,'xticklabel',[])


hold on;
%plot(xData,y2DataNN,'Linewidth',2,'Color','#E37222');
%plot(xData,y2DataODE,':','Linewidth',2,'Color','#E37222');
l3 = legend(['$x\,\,\,\omega^{50\%}$ (RAE: $',MAE1,'$\%)'],'$x \,\,\,\omega^{50\%}$ Reference',['$x\,\,\,\omega^{400\%}$NN (RAE: $',MAE3,'$\%)'],'$x\,\,\,\omega^{400\%}$ Reference','Location','northeast','FontSize',18,'Interpreter','latex');
%stra = ['NN (MAE = ',num2str(MAE1*100),' \\%% )'];

c = nexttile;
hold on;
plot(xData,y2DataNN,'Linewidth',2,'Color','#A2AD00'); % dark blue
plot(xData,y2DataODE,':','Linewidth',2,'Color','#A2AD00');

plot(xData,y4DataNN,'Linewidth',2,'Color','#0065BD'); % light blue
plot(xData,y4DataODE,':','Linewidth',2,'Color','#0065BD');


%ylim([-1, 3])

xlabel('Time $t$ (s)','Interpreter','latex','FontSize',18);
ylabel('Velocities $v$ (m/s)','Interpreter','latex','FontSize',18)
c.XGrid = 'on';
c.YGrid = 'on';
c.Box = 'on';
c.GridColor = [0 0 0];
c.GridAlpha = 0.15;
c.TickLabelInterpreter = 'latex';

hold on;
%plot(xData,y4DataNN,'Linewidth',2,'Color','#0065BD');
%plot(xData,y4DataODE,':','Linewidth',2,'Color','#0065BD');
l3 = legend(['$v\,\,\,\omega^{50\%}$ NN (RAE: $',MAE2,'$\%)'],'$v\,\,\,\omega^{50\%}$ Reference',['$v\,\,\,\omega^{400\%}$ NN (RAE: 120\%)'],'$v\,\,\,\omega^{400\%}$ Reference','Location','northeast','FontSize',18,'Interpreter','latex');


linkaxes([a c],'x')
export_fig Duffing_extrapolation_freq.pdf -transparent -pdf 

