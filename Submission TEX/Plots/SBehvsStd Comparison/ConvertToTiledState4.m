%% FIRST SBeh8 THEN Std
%% FIRST SBeh8 THEN Std
%% FIRST SBeh8 THEN Std

figure(1);

subHnd = cell(4,1);
subData = cell(4,3);
for i=1:4
    subHnd{i} =  subplot(2,2,i);
    subData{i,1} = subHnd{i}.Children(1).YData;
    subData{i,2} = subHnd{i}.Children(2).YData;
    %subData{i,3} = subHnd{i}.Title.String;
end
s1 = subplot(2,2,1);
s2 = subplot(2,2,2);
s3 = subplot(2,2,3);
s4 = subplot(2,2,4);

xData = s1.Children.XData;
y1_1DataNN = s1.Children(1).YData;
y1_2DataNN = s2.Children(1).YData;
y1_3DataNN = s3.Children(1).YData;
y1_4DataNN = s4.Children(1).YData;

y1DataODE = s1.Children(2).YData;
y2DataODE = s2.Children(2).YData;
y3DataODE = s3.Children(2).YData;
y4DataODE = s4.Children(2).YData;

MAE1_1 = sum((abs(y1_1DataNN-y1DataODE)),2)/sum(abs(y1DataODE),2)*100;
MAE1_2 = sum((abs(y1_2DataNN-y2DataODE)),2)/sum(abs(y2DataODE),2)*100;
MAE1_3 = sum((abs(y1_3DataNN-y3DataODE)),2)/sum(abs(y3DataODE),2)*100;
MAE1_4 = sum((abs(y1_4DataNN-y4DataODE)),2)/sum(abs(y4DataODE),2)*100;

figure(2);

subHnd = cell(4,1);
subData = cell(4,3);
for i=1:4
    subHnd{i} =  subplot(2,2,i);
    subData{i,1} = subHnd{i}.Children(1).YData;
    subData{i,2} = subHnd{i}.Children(2).YData;
    %subData{i,3} = subHnd{i}.Title.String;
end
s1 = subplot(2,2,1);
s2 = subplot(2,2,2);
s3 = subplot(2,2,3);
s4 = subplot(2,2,4);

xData = s1.Children.XData;
y2_1DataNN = s1.Children(1).YData;
y2_2DataNN = s2.Children(1).YData;
y2_3DataNN = s3.Children(1).YData;
y2_4DataNN = s4.Children(1).YData;

MAE2_1 = sum((abs(y2_1DataNN-y1DataODE)),2)/sum(abs(y1DataODE),2)*100;
MAE2_2 = sum((abs(y2_2DataNN-y2DataODE)),2)/sum(abs(y2DataODE),2)*100;
MAE2_3 = sum((abs(y2_3DataNN-y3DataODE)),2)/sum(abs(y3DataODE),2)*100;
MAE2_4 = sum((abs(y2_4DataNN-y4DataODE)),2)/sum(abs(y4DataODE),2)*100;

MAE1_1 = sprintf('%.2g', MAE1_1);
MAE1_2 = sprintf('%.2g', MAE1_2);
MAE1_3 = sprintf('%.2g', MAE1_3);
MAE1_4 = sprintf('%.2g', MAE1_4);

MAE2_1 = sprintf('%.2g', MAE2_1);
MAE2_2 = sprintf('%.2g', MAE2_2);
MAE2_3 = sprintf('%.2g', MAE2_3);
MAE2_4 = sprintf('%.2g', MAE2_4);

%%


f4 = figure(4);
%f4.WindowState = 'maximized';
f4.Position = [100 100 600 600];

t = tiledlayout(2,1);
t.TileSpacing = 'tight';
t.Padding = 'tight';

pause(1);
a = nexttile;
hold on;

%E37222
plot(xData,y1_1DataNN,'Linewidth',2,'Color','#A2AD00');
plot(xData,y2_1DataNN,'Linewidth',2,'Color','#E37222');
plot(xData,y1_2DataNN,'Linewidth',2,'Color','#0065BD');
plot(xData,y2_2DataNN,'Linewidth',2,'Color','#98C6EA');

plot(xData,y1DataODE,':','Linewidth',2,'Color','#808080');
plot(xData,y2DataODE,':','Linewidth',2,'Color','#CCCCCC');

%l1 = legend(stra,'Simscape','Location','northeast','FontSize',18,'Interpreter','latex');
%xlabel('$t$ [s]','Interpreter','latex','FontSize',18);
ylabel('Angles $\theta_{1,2}$ (rad)','Interpreter','latex','FontSize',18);
a.TickLabelInterpreter = 'latex';
a.XGrid = 'on';
a.YGrid = 'on';
a.Box = 'on';
a.GridColor = [0 0 0];
a.GridAlpha = 0.15;
set(a,'xticklabel',[])

%['$x$ DH NN (RAE: $',MAE1,'$\%)']
l3 = legend(['$\theta_1$ PS8 NN (RAE: $',MAE1_1,'$\%)'],['$\theta_1$ NN (RAE: $',MAE2_1,'$\%)'],['$\theta_2$ PS8 NN (RAE: $',MAE1_2,'$\%)'],['$\theta_2$ NN (RAE: $',MAE2_2,'$\%)'],'$\theta_1$ Reference','$\theta_2$ Reference','Location','northeast','FontSize',18,'Interpreter','latex');


c = nexttile;
hold on;


plot(xData,y1_3DataNN,'Linewidth',2,'Color','#A2AD00');
plot(xData,y2_3DataNN,'Linewidth',2,'Color','#E37222');
plot(xData,y1_4DataNN,'Linewidth',2,'Color','#0065BD');
plot(xData,y2_4DataNN,'Linewidth',2,'Color','#98C6EA');

plot(xData,y3DataODE,':','Linewidth',2,'Color','#808080');
plot(xData,y4DataODE,':','Linewidth',2,'Color','#CCCCCC');


xlabel('Time $t$ (s)','Interpreter','latex','FontSize',18);
ylabel('Angular Velocities $\omega_{1,2}$ (rad/s)','Interpreter','latex','FontSize',18)
c.XGrid = 'on';
c.YGrid = 'on';
c.Box = 'on';
c.GridColor = [0 0 0];
c.GridAlpha = 0.15;
c.TickLabelInterpreter = 'latex';

hold on;
l3 = legend(['$\omega_1$ PS8 NN (RAE: $',MAE1_3,'$\%)'],['$\omega_1$ NN \% (RAE: $',MAE2_3,'$\%)'],['$\omega_2$ PS8 NN (RAE: $',MAE1_4,'$\%)'],['$\omega_2$ NN (RAE: $',MAE2_4,'$\%)'],'$\omega_1$ Reference','$\omega_2$ Reference','Location','northeast','FontSize',18,'Interpreter','latex');


linkaxes([a c],'x')
export_fig prev_steps.pdf -transparent -pdf 
